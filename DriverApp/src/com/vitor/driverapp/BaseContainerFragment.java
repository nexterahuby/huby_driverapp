package com.vitor.driverapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class BaseContainerFragment extends Fragment {

	public void replaceFragment(Fragment fragment, boolean addToBackStack) 
	{
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		if (addToBackStack) 
		{
			transaction.addToBackStack(null);
		}
		
		//transaction.replace(R.id.container_framelayout, fragment);
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}
	
	public void addFragment(Fragment fragment, boolean addToBackStack,boolean setbundle,Bundle bundle) 
	{
		FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
		if (addToBackStack) 
		{
			transaction.addToBackStack(null);
		}
	
		//transaction.setCustomAnimations(R.anim.left_in,R.anim.left_out);
		//transaction.add(R.id.container_framelayout, fragment);
		if(setbundle && bundle != null)
		{
			fragment.setArguments(bundle);
		}
		
		transaction.commit();
		getChildFragmentManager().executePendingTransactions();
	}

	public boolean popFragment() {
		Log.e("test", "pop fragment: "
				+ getChildFragmentManager().getBackStackEntryCount());
		boolean isPop = false;
		if (getChildFragmentManager().getBackStackEntryCount() > 0) {
			isPop = true;
			getChildFragmentManager().popBackStack();
		}
		return isPop;
	}
}
