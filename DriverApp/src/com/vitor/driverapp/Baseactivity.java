package com.vitor.driverapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class Baseactivity extends ActionBarActivity{
	
	public static Boolean isruning=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		try
		{
		  getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	  }
	
	  public void setActiobarTitle(String title)
	    {
		   getSupportActionBar().setCustomView(R.layout.actionbar); 
	       View v = getSupportActionBar().getCustomView();
	       TextView titleTxtView = (TextView) v.findViewById(R.id.mytext);
	       titleTxtView.setText(title);
	    }
	  
	  public void setActionbartitleWithlefticon(String title,int image)
	  {
		 getSupportActionBar().setCustomView(R.layout.actionbarwithlefticon); 
	       View view = getSupportActionBar().getCustomView();
	       TextView titleTxtView = (TextView) view.findViewById(R.id.mytext);
	       ImageView imgicon=(ImageView)view.findViewById(R.id.lefticon);
	       titleTxtView.setText(title);
	       imgicon.setImageResource(image);
	  }
	  
	  protected void setActiobarLogo(int backlogo, String title)
	    {
		  getSupportActionBar().setCustomView(R.layout.actionbarback); 
	       View v = getSupportActionBar().getCustomView();
	       TextView titleTxtView = (TextView) v.findViewById(R.id.mytext);
	       ImageView imgback = (ImageView) v.findViewById(R.id.img_back);
	       imgback.setImageResource(backlogo);
	       titleTxtView.setText(title);
	       
	       imgback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	    }
	  
	  
		public  boolean isConnectingToInternet()
		{
			ConnectivityManager connectivity = (ConnectivityManager) Baseactivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
			  if (connectivity != null) 
			  {
				  NetworkInfo[] info = connectivity.getAllNetworkInfo();
				  if (info != null) 
					  for (int i = 0; i < info.length; i++) 
						  if (info[i].getState() == NetworkInfo.State.CONNECTED)
						  {
							  return true;
						  }

			  }
			  return false;
		}
	  
	  
		@Override
		protected void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
			isruning=false;
		}
		
		@Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			isruning=true;
		}

}
