package com.vitor.driverapp;

import java.util.ArrayList;

import com.vitor.driverapp.adapter.CheckoutAdapter;
import com.vitor.driverapp.services.Data;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class CheckOut extends Baseactivity implements WSResponseListener{
	
	ListView checkoutlist;
	CheckoutAdapter mCheckoutAdapter;
	HubySharedprefrence mHubySharedprefrence;
	LinearLayout lv_norecordfound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_out);
		setActiobarLogo(R.drawable.ic_back_checkout,this.getResources().getString(R.string.str_checkout));
		checkoutlist=(ListView)findViewById(R.id.checkoutlist);
		lv_norecordfound=(LinearLayout)findViewById(R.id.lv_norecordfound);
		mHubySharedprefrence=new HubySharedprefrence(CheckOut.this);    
	     Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.check_out, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	 private void Init()
	    {
		 getCheckoutOrder(mHubySharedprefrence.getLoginsession().getToken());
		 
	    }
	 
	 private void getCheckoutOrder(String tokan)
	 {
		 if(isConnectingToInternet())
		 {
			new HubbyWsWrapper(CheckOut.this).new CallRecommendGet(WEB_CALLID.driverOrdersPendingCheckout.getTypeCode(), true).execute(Appconstants.driverOrdersPendingCheckout+"&token="+tokan);
		 }else
			 DialogCaller.showDialog(CheckOut.this,CheckOut.this.getResources().getString(R.string.str_connection));
	 }

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		
		try
		{
		
		//if(ojects.getItems().size()>0)
		//{
			ArrayList<Data> data=new ArrayList<Data>();
			for(int i=0;i<ojects.getItems().size();i++)
			{
				//if(ojects.getItems().get(i).getOrderStatusID().equalsIgnoreCase("2"))
				//{
					data.add(ojects.getItems().get(i));
				//}
				
			}
			 mCheckoutAdapter=new CheckoutAdapter(CheckOut.this,data);
			 checkoutlist.setAdapter(mCheckoutAdapter);
			 if(data.size()<=0)
			 {
				 lv_norecordfound.setVisibility(View.VISIBLE);
			 }else
			 {
				 lv_norecordfound.setVisibility(View.GONE); 
			 }
		//}
		}catch(Exception e )
		{
			e.printStackTrace();
			Toast.makeText(CheckOut.this, ojects.getMessage(), Toast.LENGTH_LONG).show();
		}
		
	}
	
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		startActivity(new Intent(CheckOut.this,Homepage.class).putExtra("onbackfinish", true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		
		this.finish();
	}*/

		 	
}
