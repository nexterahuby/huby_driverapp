package com.vitor.driverapp;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

public class ChekoutOrderDetail extends Baseactivity implements WSResponseListener{
	
	TextView txt_orderno,txt_time,txt_date,txt_km,txt_source,txt_dest;
	EditText edt_toll_rs,edt_other_rs,edt_total_rs,edt_parking_rs;
	Button btn_yes;
	HubySharedprefrence mHubySharedprefrence;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chekout_order_detail);
		  setActiobarLogo(R.drawable.ic_back_arrow,this.getResources().getString(R.string.str_checkout));
		  mHubySharedprefrence=new HubySharedprefrence(this);
		  
		  txt_orderno=(TextView)findViewById(R.id.txt_orderno);
		  txt_time=(TextView)findViewById(R.id.txt_time);
		  txt_date=(TextView)findViewById(R.id.txt_date);
		  txt_km=(TextView)findViewById(R.id.txt_km);
		  txt_source=(TextView)findViewById(R.id.txt_source);
		  txt_dest=(TextView)findViewById(R.id.txt_dest);
		  edt_parking_rs=(EditText)findViewById(R.id.edt_parking_rs);
		  edt_toll_rs=(EditText)findViewById(R.id.edt_toll_rs);
		  edt_other_rs=(EditText)findViewById(R.id.edt_other_rs);
		  edt_total_rs=(EditText)findViewById(R.id.edt_total_rs);
		  
		  final Locale myLocale = new Locale("pt", "BR");
		  edt_parking_rs.addTextChangedListener(new MoneyMask(edt_parking_rs,myLocale));
		  edt_toll_rs.addTextChangedListener(new MoneyMask(edt_toll_rs,myLocale));
		  edt_other_rs.addTextChangedListener(new MoneyMask(edt_other_rs,myLocale));
		  edt_total_rs.addTextChangedListener(new MoneyMask(edt_total_rs,myLocale));
		  
		  btn_yes=(Button)findViewById(R.id.btn_yes);
		  Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.chekout_order_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void Init()
	{
		try
		  {
		  txt_orderno.setText(Appconstants.rowdata.getOrderID());
		  String[] splited = Appconstants.rowdata.getDatetime().split("\\s+");
		  txt_time.setText(splited[1]);
		  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		  Date date = format.parse(splited[0]);
		  
		  SimpleDateFormat sm = new SimpleDateFormat("dd-MMMM-yyyy");
		  String strDate = sm.format(date);
		  txt_date.setText(strDate);
		  
          txt_source.setText(Appconstants.rowdata.getSource());
		  txt_dest.setText(Appconstants.rowdata.getDestination());
		  txt_km.setText(Appconstants.rowdata.getKm());
		  
//		  edt_parking_rs.setText("$ "+Appconstants.rowdata.getCostParking());
//		  edt_toll_rs.setText("$ "+Appconstants.rowdata.getCostToll());
//		  edt_other_rs.setText("$ "+Appconstants.rowdata.getCostWaiting());
//		  edt_total_rs.setText("$ "+Appconstants.rowdata.getCostTotal());
		  
		  edt_parking_rs.setText(Appconstants.rowdata.getCostParking());
		  edt_toll_rs.setText(Appconstants.rowdata.getCostToll());
		  edt_other_rs.setText(Appconstants.rowdata.getCostWaiting());
		  edt_total_rs.setText(Appconstants.rowdata.getCostTotal());
		  
		  edt_parking_rs.setSelection(edt_parking_rs.getText().length());	
		  edt_toll_rs.setSelection(edt_toll_rs.getText().length());	
		  edt_other_rs.setSelection(edt_other_rs.getText().length());	
		  edt_total_rs.setSelection(edt_total_rs.getText().length());	
		  
		  
		  btn_yes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Validation())
				{	
					new AlertDialog.Builder(ChekoutOrderDetail.this)
					.setTitle(ChekoutOrderDetail.this.getResources().getString(R.string.str_huby) )
					.setMessage(ChekoutOrderDetail.this.getResources().getString(R.string.str_conf_checkout))
					.setIcon(R.drawable.ic_launcher)
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

					    public void onClick(DialogInterface dialog, int whichButton) {
					    	
//							Checkoutorder(mHubySharedprefrence.getLoginsession().getToken(),Appconstants.rowdata.getOrderID(),
//							edt_parking_rs.getText().toString(), Appconstants.rowdata.getCostToll(),Appconstants.rowdata.getCostWaiting(),Appconstants.rowdata.getCostTotal());
					    	
					    	
					    	if(validatecost(edt_total_rs.getText().toString().replace("R$","")))
							 {
					    		Log.d("costvalid","valided");
					    	 Checkoutorder(mHubySharedprefrence.getLoginsession().getToken(),Appconstants.rowdata.getOrderID(),
									edt_parking_rs.getText().toString().replace("R$",""),edt_toll_rs.getText().toString().replace("R$",""),edt_other_rs.getText().toString().replace("R$",""),edt_total_rs.getText().toString().replace("R$",""));
							 }else
							 {
							 DialogCaller.showDialog(ChekoutOrderDetail.this,getResources().getString(R.string.str_totaldata));
							 	}
					    }})
					 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
						}
					}).show();
				
				}
			}
		});
		  
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
		}
		
 }
	
	private void RatingDialog()
	{
		
		final Dialog dialog = new Dialog(ChekoutOrderDetail.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.dialog_rate_order);
		
		 WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		    lp.copyFrom(dialog.getWindow().getAttributes());
		    lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		    
		    dialog.getWindow().setAttributes(lp);
		  
		//dialog.setTitle("Title...");

		// set the custom dialog components - text, image and button
		  final EditText edt_addmessage=(EditText)dialog.findViewById(R.id.edt_addmessage);
		  final RatingBar ratingbar=(RatingBar)dialog.findViewById(R.id.ratingbar);
		  

		Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
		
		btn_submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				 Float rate=ratingbar.getRating();
				 String description=edt_addmessage.getText().toString();
				
				RatingOrder(mHubySharedprefrence.getLoginsession().getToken(),Appconstants.rowdata.getOrderID(),String.valueOf(rate),description);
				dialog.dismiss();
			}
		});

		dialog.show();
		
	  }
	
	
	private void Checkoutorder(String Token,String orderID,String Parking, String Toll,String Waiting,String Total)
	{
		 if(isConnectingToInternet())
		 {
			
		new HubbyWsWrapper(ChekoutOrderDetail.this).new CallRecommendGet(WEB_CALLID.CheckOutorder.getTypeCode(), true).execute(Appconstants.driverOrderCheckout+"&token="+Token+"&orderID="+orderID+"&parking="+Parking+"&toll="+Toll+"&waiting="+Waiting+"&total="+Total);
				 
			 }else
		DialogCaller.showDialog(ChekoutOrderDetail.this,ChekoutOrderDetail.this.getResources().getString(R.string.str_connection));

	}
	
	private void RatingOrder(String Token,String orderID,String Rating, String Description)
	{
		 if(isConnectingToInternet())
		 {
			 new HubbyWsWrapper(ChekoutOrderDetail.this).new CallRecommendGet(WEB_CALLID.RateOrder.getTypeCode(), true).execute(Appconstants.userRatingOrder+"&token="+Token+"&orderID="+orderID+"&rating="+Rating+"&description="+URLEncoder.encode(Description));
			 }else
			 {
				 DialogCaller.showDialog(ChekoutOrderDetail.this,ChekoutOrderDetail.this.getResources().getString(R.string.str_connection));
			 }			 	
		}
	
	 
	
	private Boolean Validation()
	{
		if(edt_parking_rs.getText().toString().equals("") || edt_parking_rs.getText().toString().equals(null))
		{
			edt_parking_rs.setText("00,0");
		}
		 if(edt_toll_rs.getText().toString().equals("") || edt_toll_rs.getText().toString().equals(null))
		{
			 edt_toll_rs.setText("00,0");
		}
		
	  if(edt_other_rs.getText().toString().equals("") || edt_other_rs.getText().toString().equals(null))
		{
		  edt_other_rs.setText("00,0");			
		}
		
		 if(edt_total_rs.getText().toString().equals("") || edt_total_rs.getText().toString().equals(null))
		{
			 edt_total_rs.setText("00,0");			
		}
		
		return true;
	}
	
	private Boolean validatecost(String value)
	{
		if(value.trim().equalsIgnoreCase("0,00") || value.trim().equalsIgnoreCase("00")) 
		{
			return false;
		}else
		{
			return true;
		}
	}
	

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		if(callId ==  4)
		{
			try
			{
				if(ojects.getErrorStatus().equalsIgnoreCase("0"))
				{
					
					startActivity(new Intent(ChekoutOrderDetail.this,OrderCheckoutcomplate.class).putExtra("Transection",0));
					this.finish();
					//RatingDialog();
				}
				else
				{
					startActivity(new Intent(ChekoutOrderDetail.this,OrderCheckoutcomplate.class).putExtra("Transection",1));
					//RatingDialog();
				}
			}catch(Exception e)
			{
				DialogCaller.showDialog(ChekoutOrderDetail.this,ojects.getMessage());
				//RatingDialog();
			}
			
		}
		
		if(callId == 8)
		{
			
		}
		
		
	}

	
	
	
}
