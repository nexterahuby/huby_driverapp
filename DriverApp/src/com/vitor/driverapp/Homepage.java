package com.vitor.driverapp;

import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

import com.vitor.driverapp.adapter.DrawerAdapter;
import com.vitor.driverapp.menu.AboutUs;
import com.vitor.driverapp.menu.ChangePass;
import com.vitor.driverapp.menu.PrivacyAndTerms;
import com.vitor.driverapp.menu.Profile;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.util.AlarmReceiver;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.GPSTracker;
import com.vitor.driverapp.util.Helper;
import com.vitor.driverapp.util.HubySharedprefrence;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Homepage extends Baseactivity implements WSResponseListener{

	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private String mActivityTitle;
	private DrawerAdapter mAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	Fragment fragment = null;
	public static Boolean isfinish = true;
	public static Boolean onbackfinish = false;
	static HubySharedprefrence mSharedprefrence;
	public static HubyDTO fromnotyobject;
	
	public static Context ctx;
	
	 Double latitude, longitude;
	 
	 public static Double order_latitude=0.0,order_longitude=0.0;
	 public static String OrderLocation="notfound";
	 
	 String url;
	 Intent mIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homepage);
		
		ctx = this;
	
		mDrawerList = (ListView) findViewById(R.id.navList);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mActivityTitle = getTitle().toString();

		addDrawerItems();
		setupDrawer();
		setActiobarTitle("Huby");
		
		mSharedprefrence=new HubySharedprefrence(this);
		mSharedprefrence.setIsnewuser(false);
		
		@SuppressWarnings("unused")
		String dashrath=mSharedprefrence.getLoginsession().getName();

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		Init();
		if(getIntent()!=null)
		{
			try
			{
			Intent intent=getIntent();
			Boolean idnoty=intent.getBooleanExtra("fromnoti", false);
			if(idnoty)
			{
				customeprogressdialog(this,fromnotyobject);
			}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			try
			{
				Intent intent=getIntent();
				onbackfinish=intent.getBooleanExtra("onbackfinish", false);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
	}

	private void Init() {
		url = "https://play.google.com/store/apps/details?id="+ this.getPackageName();
		
		
		//call service
		
		AlarmManager alarmManager=(AlarmManager) Homepage.this.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(Homepage.this, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(Homepage.this, 0, intent, 0);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),60000,pendingIntent);
		
		fragment = new HomepageFragment(this);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, fragment).addToBackStack(null)
				.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		{
			// getMenuInflater().inflate(R.menu.homepage, menu);
			return true;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		// noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}
		// Activate the navigation drawer toggle
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void addDrawerItems() {

		mAdapter = new DrawerAdapter(this);
		mDrawerList.setAdapter(mAdapter);

		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						switch (position) 
						{
						case 0:

							fragment = new HomepageFragment(Homepage.this);
							mDrawerLayout.closeDrawer(mDrawerList);
							break;
							
						case 1:
							
							isfinish = false;
							setActiobarTitle(Homepage.this.getResources().getString(R.string.str_editprofile));
							fragment = new Profile();
							mDrawerLayout.closeDrawer(mDrawerList);
							break;
							
						case 2:
							
							isfinish = false;
							setActiobarTitle(Homepage.this.getResources().getString(R.string.str_changepass));
							fragment = new ChangePass(Homepage.this);
							mDrawerLayout.closeDrawer(mDrawerList);
							break;

						case 3:
							
							isfinish = false;
							setActiobarTitle(Homepage.this.getResources().getString(R.string.str_aboutus));
							fragment = new AboutUs(Homepage.this);
							mDrawerLayout.closeDrawer(mDrawerList);
							break;

						case 4:
							
							isfinish = false;
							setActiobarTitle(Homepage.this.getResources().getString(R.string.str_privacyandterm));
							fragment = new PrivacyAndTerms(Homepage.this);
							mDrawerLayout.closeDrawer(mDrawerList);
							break;
							
						case 5:
							
							Uri uri = Uri
									.parse("https://play.google.com/store/apps/details?id="
											+ Homepage.this.getPackageName());
							// Uri uri =
							// Uri.parse("https://play.google.com/store/apps/details?id=com.mobileaction.ilife");

							Intent goToMarket = new Intent(Intent.ACTION_VIEW,
									uri);
							try {
								Homepage.this.startActivity(goToMarket);
							} catch (ActivityNotFoundException e) {
								Homepage.this.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("http://play.google.com/store/apps/details?id="
												+ Homepage.this
														.getPackageName())));
							}
							mDrawerLayout.closeDrawer(mDrawerList);
							break;

						case 6:
							
							mIntent = new Intent(Intent.ACTION_SEND);
							mIntent.setType("message/rfc822");
							mIntent.putExtra(Intent.EXTRA_EMAIL,
									new String[] { "huby@huby.com.br" });
							mIntent.putExtra(Intent.EXTRA_SUBJECT,
									"Feedback of Huby");
							try {
								Homepage.this.startActivity(Intent
										.createChooser(mIntent, "Send mail..."));
							} catch (android.content.ActivityNotFoundException ex) {
								Toast.makeText(
										Homepage.this,
										"There are no email clients installed.",
										Toast.LENGTH_SHORT).show();
							}
							mDrawerLayout.closeDrawer(mDrawerList);
							break;

						case 7:

							mIntent = new Intent(Intent.ACTION_SEND);
							mIntent.setType("text/plain");
							mIntent.putExtra(Intent.EXTRA_TEXT, url);
							mIntent.putExtra(
									android.content.Intent.EXTRA_SUBJECT,
									"Check out this App!");
							Homepage.this.startActivity(Intent.createChooser(
									mIntent, "Share App"));
							
							mDrawerLayout.closeDrawer(mDrawerList);
							break;
							
						case 8:
							mDrawerLayout.closeDrawer(mDrawerList);
							Logout();
							break;

						default:
							break;
						}

						getSupportFragmentManager().beginTransaction()
								.replace(R.id.container, fragment)
								.addToBackStack(null).commit();
						// Toast.makeText(Homepage.this, "Time for an upgrade!",
						// Toast.LENGTH_SHORT).show();
					}
				});
	}
	
	 public void Logout()
	 {
		 new AlertDialog.Builder(Homepage.this)
			.setTitle(Homepage.this.getResources().getString(R.string.str_huby) )
			.setMessage(Homepage.this.getResources().getString(R.string.str_conf_logout))
			.setIcon(R.drawable.ic_launcher)
			.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

			    public void onClick(DialogInterface dialog, int whichButton) {
			    	
			    	mSharedprefrence.setIsnewuser(true);
					startActivity(new Intent(Homepage.this,Login.class));
					finish();
			    	
			    }})
			 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			}).show();
	 }

	private void setupDrawer() {
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.string.navigation_drawer_open,
				R.string.navigation_drawer_close) {

			/** Called when a drawer has settled in a completely open state. */
			@SuppressLint("NewApi")
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getSupportActionBar().setTitle("Navigation!");
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely closed state. */
			@SuppressLint("NewApi")
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getSupportActionBar().setTitle(mActivityTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};

		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		if(onbackfinish)
		{
			this.finish();
			
		}else
		{
		super.onBackPressed();
		
		if (isfinish)
			finish();
		}
	
	}
	
	
//	public  void pinglocation()
//	{
//		
//		try {
//			GPSTracker gpsTracker = new GPSTracker(Homepage.this);
//				// check if GPS enabled
//				if (gpsTracker.canGetLocation()) 
//				{
//					latitude = gpsTracker.getLatitude();
//					longitude = gpsTracker.getLongitude();
//				} else 
//				{
//					gpsTracker.showSettingsAlert();
//				}
//			} catch (Exception e) 
//			{
//				e.printStackTrace();
//			}
//		 if(isConnectingToInternet())
//		 {
//		new HubbyWsWrapper(Homepage.this).new CallRecommendGet(WEB_CALLID.Pinglocation.getTypeCode(), true).execute(Appconstants.userPingLocation+"&token="+mSharedprefrence.getLoginsession().getToken()+"&latitude="+latitude+"&longitude="+longitude);
//		 }else
//			 DialogCaller.showDialog(Homepage.this,Homepage.this.getResources().getString(R.string.str_connection));
//		 
//		 }



	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		
		if(WEB_CALLID.UpdateDriverInvite.getTypeCode()==callId)
		{
			//
			
		
		}
	}
	
	
	public static void getLocationAndLatlong()
	{
		GPSTracker gpsTracker;
		try {
			gpsTracker = new GPSTracker(ctx);
			
			order_latitude=gpsTracker.getLatitude();
			order_longitude=gpsTracker.getLongitude();
			
			OrderLocation=getAddressFromLocation(order_latitude,order_longitude);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 public static  String getAddressFromLocation(Double lat, Double lng) 
		{
		 StringBuilder sb = new StringBuilder();
			try {
				Geocoder geocoder = new Geocoder(ctx, Locale.ENGLISH);
				List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
				
				for(int i=0;i<addresses.get(0).getMaxAddressLineIndex();i++)
				{
					sb.append(addresses.get(0).getAddressLine(i)).append(" - ");
				}

			} catch (Exception e) 
			{
				e.printStackTrace();
				return "";
			}
			
			return sb.toString().substring(0,sb.toString().length()-2).replace("State of","");
		}
	 
	 public static  String getPostalcode(Double lat, Double lng) 
		{
		 try {
			Geocoder geocoder = new Geocoder(ctx, Locale.ENGLISH);
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			return addresses.get(0).getPostalCode();

		} catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}
		}
	
	
	 public static  void customeprogressdialog(Context context,final HubyDTO ojects)
		{
			final Dialog dialog=new Dialog(Homepage.ctx);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			dialog.setContentView(R.layout.dialog_acceptorder);
			
			getLocationAndLatlong();
			
			TextView txt_source =(TextView)dialog.findViewById(R.id.txt_source);
			TextView txt_destination =(TextView)dialog.findViewById(R.id.txt_destination);
			TextView txt_name=(TextView)dialog.findViewById(R.id.txt_name);
			TextView txt_comp=(TextView)dialog.findViewById(R.id.txt_comp);
			TextView txt_payment_type=(TextView)dialog.findViewById(R.id.txt_payment_type);
			TextView txt_date_time=(TextView)dialog.findViewById(R.id.txt_date_time);
			
			try
			{
				txt_source.setText(ojects.getInvites().get(0).getSource());
				txt_destination.setText(ojects.getInvites().get(0).getDestination());
				txt_name.setText(ojects.getInvites().get(0).getUser());
				txt_comp.setText(ojects.getInvites().get(0).getOrganization());
				txt_payment_type.setText(ojects.getInvites().get(0).getPayment());
				txt_date_time.setText(ojects.getInvites().get(0).getDatetime());
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			ImageButton imb_accept=(ImageButton)dialog.findViewById(R.id.imb_accept);
			ImageButton imb_reject=(ImageButton)dialog.findViewById(R.id.imb_reject);
			ImageView img_map=(ImageView)dialog.findViewById(R.id.img_map);
			
			imb_reject.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					callupdatedriverinvite(mSharedprefrence.getLoginsession().getToken(),mSharedprefrence.getLoginsession().getUserID(),
							ojects.getInvites().get(0).getOrderID(),0,order_latitude,order_longitude,OrderLocation);
					//not open dialog again
					Helper.ordergetonce.put(ojects.getInvites().get(0).getOrderID(),ojects.getInvites().get(0).getOrderID());
					
					dialog.dismiss();
				}
			});
			
			imb_accept.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					callupdatedriverinvite(mSharedprefrence.getLoginsession().getToken(),mSharedprefrence.getLoginsession().getUserID(),
							ojects.getInvites().get(0).getOrderID(),1,order_latitude,order_longitude,OrderLocation);
					//not open dialog again
					Helper.ordergetonce.put(ojects.getInvites().get(0).getOrderID(),ojects.getInvites().get(0).getOrderID());
					
					dialog.dismiss();
				}
			});
			
			img_map.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Appconstants.invitedata=ojects.getInvites().get(0);
					ctx.startActivity(new Intent(ctx,InviteDetail.class));
				}
			});
			
			WindowManager.LayoutParams lp =new WindowManager.LayoutParams();
			lp.copyFrom(dialog.getWindow().getAttributes());
			lp.width=WindowManager.LayoutParams.MATCH_PARENT;
			lp.height=WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity=Gravity.CENTER;
			Window window=dialog.getWindow();
			window.setAttributes(lp);
			dialog.show();
			
		}
	 
	 
	 @SuppressWarnings("deprecation")
	public static  void callupdatedriverinvite(String Tokan,String driverID,String orderID,int Accept,Double Latitude,Double Longitude,String Location)
	 {
		 new HubbyWsWrapper(ctx).new CallRecommendGet(WEB_CALLID.UpdateDriverInvite.getTypeCode(), true).execute(Appconstants.updateDriverInvite+"&Token="+URLEncoder.encode(Tokan)+"&driverID="+URLEncoder.encode(driverID)+"&orderID="+URLEncoder.encode(orderID)+"&Accept="+Accept+"&Latitude="+URLEncoder.encode(Latitude.toString())+"&Longitude="+URLEncoder.encode(Longitude.toString())+"&Local="+URLEncoder.encode(Location));                         
		
	 }
	 
	
}
