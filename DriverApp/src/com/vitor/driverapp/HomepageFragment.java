package com.vitor.driverapp;

import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.GPSTracker;
import com.vitor.driverapp.util.HubySharedprefrence;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class HomepageFragment extends BaseContainerFragment implements WSResponseListener
{
	HubySharedprefrence mSharedprefrence;
	 RelativeLayout rv_myschedule,rv_orders,rv_checkout;
	ActionBarActivity activity;
	TextView txt_drivername;
	TextView txt_driverstatus;
	ImageButton imb_driverstatus;
	
	Boolean driverstatus=true;
	
	Double latitude, longitude;

	public HomepageFragment(ActionBarActivity activity) 
	{
		this.activity=activity;
	
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_homepage, container, false);
		mSharedprefrence=new HubySharedprefrence(activity);
		rv_myschedule=(RelativeLayout)v.findViewById(R.id.rv_myschedule);
		rv_orders=(RelativeLayout)v.findViewById(R.id.rv_orders);
		rv_checkout=(RelativeLayout)v.findViewById(R.id.rv_checkout);
		txt_drivername=(TextView)v.findViewById(R.id.txt_drivername);
		txt_driverstatus=(TextView)v.findViewById(R.id.txt_driverstatus);
		imb_driverstatus=(ImageButton)v.findViewById(R.id.imb_driverstatus);
		
		
		String dashrath=mSharedprefrence.getLoginsession().getName();
		Log.d("dashrath",dashrath);
		
		String[] names =dashrath.split(" ");
		txt_drivername.setText(names[0]);
		
		
		
		   activity.getSupportActionBar().setCustomView(R.layout.actionbar); 
	       View view = activity.getSupportActionBar().getCustomView();
	       ImageView imgicon=(ImageView)view.findViewById(R.id.lefticon);
	       TextView titleTxtView = (TextView) view.findViewById(R.id.mytext);
	       titleTxtView.setText(this.getResources().getString(R.string.str_huby));
	       imgicon.setImageResource(R.drawable.ic_launcher);
		
		 Init();
		return v;
	}
	
	private void Init()
	{
		
		rv_myschedule.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),MySchedule.class));
			}
		});
		
		rv_orders.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),Orders.class));
			}
		});
		
		rv_checkout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getActivity(),CheckOut.class));
			}
		});
		
		
		imb_driverstatus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					GPSTracker gpsTracker = new GPSTracker(getActivity());
					String address = null;
					
						// check if GPS enabled
						if (gpsTracker.canGetLocation()) 
						{
							latitude = gpsTracker.getLatitude();
							longitude = gpsTracker.getLongitude();
							
							 address= getAddressFromLocation(latitude, longitude);
						} else 
						{
							gpsTracker.showSettingsAlert();
						}
				
				
				if(driverstatus)
				{
					imb_driverstatus.setBackgroundResource(R.drawable.ic_unavail_btn);
					txt_driverstatus.setText(HomepageFragment.this.getResources().getString(R.string.str_unavail));
					UpdateDriverstatus(mSharedprefrence.getLoginsession().getToken(),"0",address, latitude, longitude);
					driverstatus=false;
				}else
				{
					imb_driverstatus.setBackgroundResource(R.drawable.ic_avail_btn);
				   txt_driverstatus.setText(HomepageFragment.this.getResources().getString(R.string.str_avail));
				   UpdateDriverstatus(mSharedprefrence.getLoginsession().getToken(),"1",address, latitude, longitude);
					driverstatus=true;
				 
				}
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	private String getAddressFromLocation(Double lat, Double lng) 
	{

		try {
			Geocoder geocoder = new Geocoder(activity, Locale.ENGLISH);
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			
			return addresses.get(0).getAddressLine(1);

		} catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		
		Homepage.isfinish=true;
	}


	private void UpdateDriverstatus(String Token,String StatusId,String local, Double latitude,Double longitude)
	{

		 if(((Homepage)getActivity()).isConnectingToInternet())
				 {
		new HubbyWsWrapper(getActivity(),HomepageFragment.this).new CallRecommendGet(WEB_CALLID.Driver_updatestatus.getTypeCode(), true).execute(Appconstants.driverUpdateStatus+"&token="+Token+"&statusID="+StatusId+"&local="+URLEncoder.encode(local)+"&latitude="+latitude+"&longitude="+longitude);
				 }else 
					 DialogCaller.showDialog(getActivity(),getActivity().getResources().getString(R.string.str_connection));
				 
			}
	
	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		
	}
	

}