package com.vitor.driverapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.w3c.dom.Document;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DirectionsJSONParser;
import com.vitor.driverapp.util.GMapV2GetRouteDirection;
import com.vitor.driverapp.util.HubySharedprefrence;

import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class InviteDetail  extends Baseactivity implements LocationListener{
	
	
	
	// Google Map
    private GoogleMap googleMap;
    GMapV2GetRouteDirection v2GetRouteDirection;
    LatLng fromPosition;
    LatLng toPosition;
    Document document;
    private LocationManager locationManager;
    HubySharedprefrence hubySharedprefrence;
    PolygonOptions opts;
    
    ProgressBar pbar;
       
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_invite_detail);
		hubySharedprefrence=new HubySharedprefrence(this);
		  
		  setActiobarLogo(R.drawable.ic_back_order,this.getResources().getString(R.string.str_order_no)+" "+Appconstants.invitedata.getOrderID());
		Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.myschedule_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void Init()
	{
		
		try {
            // Loading map
			
			pbar=(ProgressBar)findViewById(R.id.pbar);
            initilizeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}


	/**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
        	
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.invite_map)).getMap();
            
            locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
         
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setAllGesturesEnabled(true);
            googleMap.setTrafficEnabled(true);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
            
            
            googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }
                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.marker, null);
                    TextView info= (TextView) v.findViewById(R.id.info);
                    TextView infodetail= (TextView) v.findViewById(R.id.infodetail);
                    info.setText(marker.getTitle());
                    infodetail.setText(marker.getSnippet());
                    return v;
                }
            });
            
            
            try
		     {
		     String[] sourcelatlong = Appconstants.invitedata.getSourceLatLng().split(",");
		     double slatitude = Double.parseDouble(sourcelatlong[0]);
		     double slongitude = Double.parseDouble(sourcelatlong[1]);
		     
		     String[] destilatlong = Appconstants.invitedata.getDestinationLatLng().split(",");
		     double dlatitude = Double.parseDouble(destilatlong[0]);
		     double dlongitude = Double.parseDouble(destilatlong[1]);
		     
//		        LatLng origin = new LatLng();
//				LatLng dest = new LatLng();
            
            fromPosition = new LatLng(slatitude,slongitude);
            toPosition = new LatLng(dlatitude,dlongitude);
            
            
             opts=new PolygonOptions();
             
             ArrayList<LatLng> list = new ArrayList<LatLng>();
             list.add(fromPosition);
             list.add(toPosition);
             

            for (LatLng location : list) {
              opts.add(location);
            }
            
            
		     }catch(Exception e){e.printStackTrace();}
            
           // GetRouteTask getRoute = new GetRouteTask();
           // getRoute.execute();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
    
    
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
		 LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
		    googleMap.animateCamera(cameraUpdate);
		    locationManager.removeUpdates(this);
		    
		    pbar.setVisibility(View.VISIBLE);
		    
		     try
		     {
		     String[] sourcelatlong = Appconstants.invitedata.getSourceLatLng().split(",");
		     double slatitude = Double.parseDouble(sourcelatlong[0]);
		     double slongitude = Double.parseDouble(sourcelatlong[1]);
		     
		     String[] destilatlong = Appconstants.invitedata.getDestinationLatLng().split(",");
		     double dlatitude = Double.parseDouble(destilatlong[0]);
		     double dlongitude = Double.parseDouble(destilatlong[1]);
		     
		        LatLng origin = new LatLng(slatitude,slongitude);
				LatLng dest = new LatLng(dlatitude,dlongitude);
		     
		     cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(slatitude,slongitude), 12);
		     googleMap.animateCamera(cameraUpdate);
		     
		     MarkerOptions marker = new MarkerOptions().position(origin).icon(BitmapDescriptorFactory.fromResource(R.drawable.map)).title("Origem").snippet(Appconstants.invitedata.getSource());
			    googleMap.addMarker(marker);
			    
			    MarkerOptions marker1 = new MarkerOptions().position(dest).icon(BitmapDescriptorFactory.fromResource(R.drawable.map)).title("Destino").snippet(Appconstants.invitedata.getDestination());
			    googleMap.addMarker(marker1);
		     
		     
				 route(origin,dest);
	         } catch (Exception e) {
						// TODO: handle exception
					}

	// Get back the mutable Polyline
	//Polyline polyline = googleMap.addPolyline(rectOptions);
		
	}
	
	
	public void route(LatLng origin,LatLng dest)
	{
		
		// Getting URL to the Google Directions API
		String url = getDirectionsUrl(origin, dest);				
		
		DownloadTask downloadTask = new DownloadTask();
		
		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	
//	private void CancelOrder(String Tokan,String Orderid,String Reasonid)
//	{
//		 if(isConnectingToInternet())
//		 {
//		new HubbyWsWrapper(MyscheduleDetail.this).new CallRecommendGet(WEB_CALLID.CancelOrder.getTypeCode(), true).execute(Appconstants.userCancelOrder+"&token="+Tokan+"&orderID="+Orderid+"&reasonID="+Reasonid);
//		 }else
//			 DialogCaller.showDialog(MyscheduleDetail.this,MyscheduleDetail.this.getResources().getString(R.string.str_connection));
//	 }
	
	

	
	
	
	/////////////////map By dj/////////////////////
	
	// Fetches data from url passed
		private class DownloadTask extends AsyncTask<String, Void, String>{			
					
			// Downloading data in non-ui thread
			@Override
			protected String doInBackground(String... url) {
					
				// For storing data from web service
				String data = "";
						
				try{
					// Fetching the data from web service
					data = downloadUrl(url[0]);
				}catch(Exception e){
					Log.d("Background Task",e.toString());
				}
				return data;		
			}
			
			// Executes in UI thread, after the execution of
			// doInBackground()
			@Override
			protected void onPostExecute(String result) {			
				super.onPostExecute(result);			
				
				ParserTask parserTask = new ParserTask();
				
				// Invokes the thread for parsing the JSON data
				parserTask.execute(result);
					
			}		
		}
		
		/** A method to download json data from url */
	    private String downloadUrl(String strUrl) throws IOException{
	        String data = "";
	        InputStream iStream = null;
	        HttpURLConnection urlConnection = null;
	        try{
	                URL url = new URL(strUrl);

	                // Creating an http connection to communicate with url 
	                urlConnection = (HttpURLConnection) url.openConnection();

	                // Connecting to url 
	                urlConnection.connect();

	                // Reading data from url 
	                iStream = urlConnection.getInputStream();

	                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

	                StringBuffer sb  = new StringBuffer();

	                String line = "";
	                while( ( line = br.readLine())  != null){
	                        sb.append(line);
	                }
	                
	                data = sb.toString();

	                br.close();

	        }catch(Exception e){
	                Log.d("Exception while downloading url", e.toString());
	        }finally{
	                iStream.close();
	                urlConnection.disconnect();
	        }
	        return data;
	     }
	    
	    
		/** A class to parse the Google Places in JSON format */
	    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
	    	
	    	// Parsing the data in non-ui thread    	
			@Override
			protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
				
				JSONObject jObject;	
				List<List<HashMap<String, String>>> routes = null;			           
	            
	            try{
	            	jObject = new JSONObject(jsonData[0]);
	            	DirectionsJSONParser parser = new DirectionsJSONParser();
	            	
	            	// Starts parsing data
	            	routes = parser.parse(jObject);    
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	            return routes;
			}
			
			// Executes in UI thread, after the parsing process
			@Override
			protected void onPostExecute(List<List<HashMap<String, String>>> result) {
				ArrayList<LatLng> points = null;
				PolylineOptions lineOptions = null;
				MarkerOptions markerOptions = new MarkerOptions();
				
				// Traversing through all the routes
				for(int i=0;i<result.size();i++){
					points = new ArrayList<LatLng>();
					lineOptions = new PolylineOptions();
					
					// Fetching i-th route
					List<HashMap<String, String>> path = result.get(i);
					
					// Fetching all the points in i-th route
					for(int j=0;j<path.size();j++){
						HashMap<String,String> point = path.get(j);					
						
						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);	
						
						points.add(position);						
					}
					
					// Adding all the points in the route to LineOptions
					lineOptions.addAll(points);
					lineOptions.width(5);
					lineOptions.color(Color.RED);	
					
				}
				
				// Drawing polyline in the Google Map for the i-th route
				googleMap.addPolyline(lineOptions);		
				
				  pbar.setVisibility(View.GONE);
			}			
	    }   
	    
	    
	    private String getDirectionsUrl(LatLng origin,LatLng dest){
			
			// Origin of route
			String str_origin = "origin="+origin.latitude+","+origin.longitude;
			
			// Destination of route
			String str_dest = "destination="+dest.latitude+","+dest.longitude;		
			
						
			// Sensor enabled
			String sensor = "sensor=false";			
						
			// Building the parameters to the web service
			String parameters = str_origin+"&"+str_dest+"&"+sensor;
						
			// Output format
			String output = "json";
			
			// Building the url to the web service
			String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
			
			return url;
		}
	    
	    
	    @Override
	    public void onBackPressed() {
	    	// TODO Auto-generated method stub
	    	super.onBackPressed();
	    }
	    
	
	}
