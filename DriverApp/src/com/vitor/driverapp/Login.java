package com.vitor.driverapp;

import com.vitor.driverapp.bean.LoginBean;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

import android.telephony.TelephonyManager;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Baseactivity  implements WSResponseListener{

	EditText edt_phonenumber;
	EditText edt_password;
	Button btn_login;
	HubySharedprefrence mSharedprefrence;
	TextView txt_forgotpass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		edt_phonenumber=(EditText)findViewById(R.id.edt_phonenumber);
		edt_phonenumber.addTextChangedListener(new BrazilPhoneMask(edt_phonenumber));
		edt_password=(EditText)findViewById(R.id.edt_password);
		btn_login=(Button)findViewById(R.id.btn_login);
		txt_forgotpass=(TextView)findViewById(R.id.txt_forgotpass);

		Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void Init()
	{
		try
		{
			TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
			String number = tm.getLine1Number();
			if(number.equals(null))
			{
				edt_phonenumber.setEnabled(true);
			}else
			{
				String A=number.substring(2);
				edt_phonenumber.setText(A);

			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			edt_phonenumber.setEnabled(true);
		}

		mSharedprefrence = new HubySharedprefrence(Login.this);
		btn_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(Validation())
				{
					DriverLogin(edt_phonenumber.getText().toString(),edt_password.getText().toString());
				}

			}
		});

		txt_forgotpass.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ForgotPassDialog();
			}
		});

	}



	private void ForgotPassDialog()
	{
		final Dialog dialog = new Dialog(Login.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.dialog_forgotpass);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

		dialog.getWindow().setAttributes(lp);

		final EditText edt_phonenumber=(EditText)dialog.findViewById(R.id.edt_phonenumber);

		Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);


		btn_submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ForgotPassValidation(edt_phonenumber))
				{
					ForgotPass(mSharedprefrence.getLoginsession().getToken(),edt_phonenumber.getText().toString());
					dialog.dismiss();
				}
			}
		});

		dialog.show();

	}



	private void ForgotPass(String Tokan,String Phone)
	{
		if(isConnectingToInternet())
		{
			//new HubbyWsWrapper(Login.this).new CallRecommendGet(WEB_CALLID.Forgotpass.getTypeCode(), true).execute(Appconstants.userForgotPassword+"&phone="+"27888888888");
			new HubbyWsWrapper(Login.this).new CallRecommendGet(WEB_CALLID.Forgotpass.getTypeCode(), true).execute(Appconstants.driverForgotPassword+"&email="+Phone);
		}else {
			DialogCaller.showDialog(Login.this,Login.this.getResources().getString(R.string.str_connection));
		}
	}

	private Boolean Validation()
	{
		if(edt_phonenumber.getText().toString().equals("") || edt_phonenumber.getText().toString().equals(null))
		{
			edt_phonenumber.setError(this.getResources().getString(R.string.str_req_phone));
			edt_phonenumber.requestFocus();
			return false;
		}

		if(edt_phonenumber.getText().toString().equals("") || edt_phonenumber.getText().toString().equals(null))
		{
			edt_phonenumber.setError(this.getResources().getString(R.string.str_req_phone));
			edt_phonenumber.requestFocus();
			return false;
		}
		else if(edt_password.getText().toString().equals("") || edt_password.getText().toString().equals(null))
		{
			edt_password.setError(this.getResources().getString(R.string.str_req_pass));
			edt_password.requestFocus();
			return false;
		}

		return true;
	}


	private Boolean ForgotPassValidation(EditText editText)
	{
		if(editText.getText().toString().equals("") || editText.getText().toString().equals(null))
		{
			editText.setError(this.getResources().getString(R.string.str_req_email));
			editText.requestFocus();
			return false;
		}
		else if(!Appconstants.emailValidator(editText.getText().toString()))
		{
			editText.setError(this.getResources().getString(R.string.str_req_validemail));
			editText.requestFocus();
			return false;
		}

		return true;
	}

	private void DriverLogin(String phone,String password)
	{

		phone=phone.replace("(","").replace(")", "").replace("-","").replace(" ","");

		if(isConnectingToInternet())
		{
			//new HubbyWsWrapper(Login.this).new CallRecommendGet(WEB_CALLID.DriverLogin.getTypeCode(), true).execute(Appconstants.driverLogin+"&login="+"27999999999"+"&pass=123456");
			new HubbyWsWrapper(Login.this).new CallRecommendGet(WEB_CALLID.DriverLogin.getTypeCode(), true).execute(Appconstants.driverLogin+"&login="+phone+"&pass="+password);
		}else
			DialogCaller.showDialog(Login.this,Login.this.getResources().getString(R.string.str_connection));
	}

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub

		HubyDTO ojects = (HubyDTO) object;
		try
		{
			if(callId==1)
			{
				if(ojects.getStatus().equalsIgnoreCase("1"))
				{
					LoginBean bean=new LoginBean();

					bean.setName(ojects.getName());
					bean.setOrganizationID(ojects.getOrganizationID());
					bean.setUserID(ojects.getDriverID());
					bean.setOrganization(ojects.getOrganization());
					bean.setEmail(ojects.getEmail());
					bean.setPhone(ojects.getPhone());
					bean.setTransactions( ojects.getTransactions());
					bean.setBalance( ojects.getBalance());
					bean.setAppointments(ojects.getAppointments());
					bean.setToken(ojects.getToken());

					mSharedprefrence.setLoginsession(bean);

					if(ojects.getToken().equalsIgnoreCase("") || null == ojects.getToken())
					{
						DialogCaller.showDialog(Login.this, getString(R.string.str_Authentication));
					}else
					{
						startActivity(new Intent(Login.this,Homepage.class));
						Login.this.finish();
					}
				}
				else
				{
					DialogCaller.showDialog(Login.this,ojects.getMessage());
				}

			}else if(callId==11)
			{
				try
				{
					if(ojects.getErrorStatus().equalsIgnoreCase("1") )
					{
						DialogCaller.showDialog(Login.this, ojects.getMessage());
					}else if(ojects.getErrorStatus().equalsIgnoreCase("0"))
					{
						DialogCaller.showDialog(Login.this, ojects.getMessage());
					}

				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}

		}catch(Exception e)
		{
			e.printStackTrace();
			Toast.makeText(Login.this, e.getMessage()+" ", Toast.LENGTH_LONG).show();
		}
	}
}
