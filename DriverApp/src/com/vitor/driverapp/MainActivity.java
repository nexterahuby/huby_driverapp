package com.vitor.driverapp;


import com.vitor.driverapp.util.HubySharedprefrence;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity {
	
	HubySharedprefrence hubySharedprefrence;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		hubySharedprefrence=new HubySharedprefrence(this);
		
		try
		{
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				
				if(hubySharedprefrence.getIsnewuser())
				{
					startActivity(new Intent(MainActivity.this,Login.class));
					finish();
				}else
				{
				startActivity(new Intent(MainActivity.this,Homepage.class));
				finish();
				}
				
			}
		}, 2000);
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
