package com.vitor.driverapp;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vitor.driverapp.adapter.TabsPagerAdapter;
import com.vitor.driverapp.services.Data;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class MySchedule extends Baseactivity implements TabListener, WSResponseListener{
	
	 private ViewPager viewPager;
	 private TabsPagerAdapter mAdapter;
	 private ActionBar actionBar;
	 HubySharedprefrence   mHubySharedprefrence; 
	 
	 ArrayList<Data> data=new ArrayList<Data>();
	
	 private String[] tabs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_schedule);
		mHubySharedprefrence=new HubySharedprefrence(this);
		    viewPager = (ViewPager) findViewById(R.id.pager);
	        actionBar = getSupportActionBar();
	        
	        tabs=this.getResources().getStringArray(R.array.tabs); 
	        
	        setActiobarLogo(R.drawable.ic_back_sedule,this.getResources().getString(R.string.str_myschedule));
	        
	        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);        
	 
	        // Adding Tabs
	        int pos=0;
	        for (String tab_name : tabs) {
	        	
	        	TextView tView=new TextView(getApplicationContext());
	    		tView.setText(tab_name);
	    		if(pos==0)
	    		{
	    		tView.setTextColor(Color.parseColor("#ffff8800")); 
	    		}else if(pos==1)
	    		{
	    			tView.setTextColor(Color.parseColor("#006400")); 
	    		}else if(pos==2)
	    		{
	    			tView.setTextColor(Color.parseColor("#b20000")); 
	    		}else if(pos==3)
	    		{
	    			tView.setTextColor(Color.parseColor("#ff33b5e5")); 
	    		}
	    		
	    		tView.setTextSize(15);
	    		tView.setTypeface(null,Typeface.NORMAL);
	    		tView.setAllCaps(true);
	    		
	            actionBar.addTab(actionBar.newTab().setCustomView(tView).setTabListener(this));
	            pos++;
	        }
	        
	        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
	        	 
	            @Override
	            public void onPageSelected(int position) {
	                // on changing the page
	                // make respected tab selected
	                actionBar.setSelectedNavigationItem(position);
	            }
	 
	            @Override
	            public void onPageScrolled(int arg0, float arg1, int arg2) {
	            }
	 
	            @Override
	            public void onPageScrollStateChanged(int arg0) {
	            }
	        });
	        
	        Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.my_schedule, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		viewPager.setCurrentItem(tab.getPosition());	

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		viewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	private void Init()
	{
		getOrderList(mHubySharedprefrence.getLoginsession().getToken());
	}
	
	 private void getOrderList(String tokan)
	 {
		 if(isConnectingToInternet())
		 {
			new HubbyWsWrapper(MySchedule.this).new CallRecommendGet(WEB_CALLID.OrderList.getTypeCode(), true).execute(Appconstants.userOrdersList+"&token="+tokan);
		 }else
		 {
			 DialogCaller.showDialog(MySchedule.this,MySchedule.this.getResources().getString(R.string.str_connection));
		 }
		 
		}
		 

	@Override
	public void onPostResponse(Object object, int callId) {
		
		HubyDTO ojects = (HubyDTO) object;
		// TODO Auto-generated method stub
		
		try
		{
		if(ojects.getItems().size()>0)
		{
			data=new ArrayList<Data>();
			
			for(int i=0;i<ojects.getItems().size();i++)
			{
				data.add(ojects.getItems().get(i));
			}
			  mAdapter = new TabsPagerAdapter(getSupportFragmentManager(),"schedule",data);	
			  viewPager.setAdapter(mAdapter);	
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			Toast.makeText(MySchedule.this, ojects.getMessage(), Toast.LENGTH_LONG).show();
		}
		
	}
}
