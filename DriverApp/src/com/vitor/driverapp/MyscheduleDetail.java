package com.vitor.driverapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.w3c.dom.Document;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.DirectionsJSONParser;
import com.vitor.driverapp.util.GMapV2GetRouteDirection;
import com.vitor.driverapp.util.HubySharedprefrence;

public class MyscheduleDetail extends Baseactivity implements LocationListener,WSResponseListener{
	
	Button btn_reject,btn_driver_aceept;
	LinearLayout lv_footer;
	TextView txt_comment;
	
	// Google Map
    private GoogleMap googleMap;
    GMapV2GetRouteDirection v2GetRouteDirection;
    LatLng fromPosition;
    LatLng toPosition;
    Document document;
    private LocationManager locationManager;
    HubySharedprefrence hubySharedprefrence;
    PolygonOptions opts;
    ImageView img_user;
    
    LinearLayout costinfo_lable, lv_costinfo;
    
    int reasonid=0;
    
    ScrollView myscroll;
    
    TextView txt_orderno,txt_time,txt_date,txt_km,txt_source,txt_dest,txt_status;
    
	TextView txt_carcolor, txt_carmenufactor, txt_carmodel, txt_carplate;
    
    TextView txt_total, txt_other, txt_toll, txt_parking;
    TextView txt_user_name,txt_company_name;
    
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myschedule_detail);
		hubySharedprefrence=new HubySharedprefrence(this);
		  txt_orderno=(TextView)findViewById(R.id.txt_orderno);
		  txt_time=(TextView)findViewById(R.id.txt_time);
		  txt_date=(TextView)findViewById(R.id.txt_date);
		  txt_km=(TextView)findViewById(R.id.txt_km);
		  txt_source=(TextView)findViewById(R.id.txt_source);
		  txt_dest=(TextView)findViewById(R.id.txt_dest);
		  lv_footer=(LinearLayout)findViewById(R.id.lv_footer);
		  txt_comment=(TextView)findViewById(R.id.txt_comment);
		  txt_status=(TextView)findViewById(R.id.txt_status);
		  myscroll=(ScrollView)findViewById(R.id.myscroll);
		  
		    txt_total=(TextView)findViewById(R.id.txt_total);
			txt_other=(TextView)findViewById(R.id.txt_other);
			txt_toll=(TextView)findViewById(R.id.txt_toll);
			txt_parking=(TextView)findViewById(R.id.txt_parking);
			
			txt_carcolor=(TextView)findViewById(R.id.txt_carcolor);
			txt_carmenufactor=(TextView)findViewById(R.id.txt_carmenufactor);
			txt_carmodel=(TextView)findViewById(R.id.txt_carmodel);
			txt_carplate=(TextView)findViewById(R.id.txt_carplate);
			
			img_user=(ImageView)findViewById(R.id.img_user);
		
		btn_reject=(Button)findViewById(R.id.btn_reject);
		btn_driver_aceept=(Button)findViewById(R.id.btn_driver_aceept);
		
		costinfo_lable=(LinearLayout)findViewById(R.id.costinfo_lable);
		lv_costinfo=(LinearLayout)findViewById(R.id.lv_costinfo);
		txt_user_name=(TextView)findViewById(R.id.txt_user_name);
		txt_company_name=(TextView)findViewById(R.id.txt_company_name);
		
		  setActiobarLogo(R.drawable.ic_back_order,this.getResources().getString(R.string.str_order_no)+" "+Appconstants.shedulerowdata.getOrderID());
		  
		Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.myschedule_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private void Init()
	{
		
		try
		  {
			if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("0"))
			{
				lv_footer.setVisibility(View.VISIBLE);
				
			}
			else
			{
				lv_footer.setVisibility(View.GONE);
				LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT,
                        LayoutParams.MATCH_PARENT, 1.0f);
				myscroll.setLayoutParams(param);
			}
			
			
			if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("2"))
			{
				costinfo_lable.setVisibility(View.VISIBLE);
				lv_costinfo.setVisibility(View.VISIBLE);
			}else
			{
				costinfo_lable.setVisibility(View.GONE);
				lv_costinfo.setVisibility(View.GONE);
			}
			
		  txt_status.setText(Appconstants.shedulerowdata.getOrderStatus());
		  txt_orderno.setText(Appconstants.shedulerowdata.getOrderID());
		  txt_user_name.setText(Appconstants.shedulerowdata.getUser());
		  txt_company_name.setText(Appconstants.shedulerowdata.getOrganization());
		  String[] splited = Appconstants.shedulerowdata.getDatetime().split("\\s+");
		  txt_time.setText(splited[1]);
		  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		  Date date = format.parse(splited[0]);
		  
		  SimpleDateFormat sm = new SimpleDateFormat("dd-MMMM-yyyy");
		  String strDate = sm.format(date);
		  txt_date.setText(strDate);
		  
          txt_source.setText(Appconstants.shedulerowdata.getSource());
		  txt_dest.setText(Appconstants.shedulerowdata.getDestination());
		  txt_km.setText(Appconstants.shedulerowdata.getKm());
		  txt_comment.setText(Html.fromHtml(Appconstants.shedulerowdata.getInstructions()));
		  
			txt_total.setText(Appconstants.shedulerowdata.getCostTotal());
			txt_other.setText(Appconstants.shedulerowdata.getCostWaiting());
			txt_toll.setText(Appconstants.shedulerowdata.getCostToll());
			txt_parking.setText(Appconstants.shedulerowdata.getCostParking());
			
			
			txt_carcolor.setText(Appconstants.shedulerowdata.getCarColor());
			txt_carmenufactor.setText(Appconstants.shedulerowdata.getCarManufacturer());
			txt_carmodel.setText(Appconstants.shedulerowdata.getCarModel());
			txt_carplate.setText(Appconstants.shedulerowdata.getCarPlate());
			
			if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("3"))
			{
				img_user.setImageResource(R.drawable.bg_circle_red);
			}else if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("0"))
			{
				img_user.setImageResource(R.drawable.bg_circle_yellow);
			}else if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("1"))
			{
				img_user.setImageResource(R.drawable.bg_circle_green);
			}
			
			else if(Appconstants.shedulerowdata.getOrderStatusID().equalsIgnoreCase("2"))
			{
				img_user.setImageResource(R.drawable.bg_circle_green);
			}else
			{
				img_user.setImageResource(R.drawable.bg_circle_green);
			}
		
		  }catch (Exception e) {
				// TODO: handle exception
				  e.printStackTrace();
			}

		try {
            // Loading map
            initilizeMap();
 
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		btn_reject.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RejectOrder(hubySharedprefrence.getLoginsession().getToken(),Appconstants.shedulerowdata.getOrderID());
			}
		});
		
		btn_driver_aceept.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AcceptOrder(hubySharedprefrence.getLoginsession().getToken(),Appconstants.shedulerowdata.getOrderID());
			}
		});
	}
	
	
	
	private void AcceptOrder(final String Tokan,final String orderID)
	{
		new AlertDialog.Builder(MyscheduleDetail.this)
		.setTitle(MyscheduleDetail.this.getResources().getString(R.string.str_huby) )
		.setMessage(MyscheduleDetail.this.getResources().getString(R.string.str_conf_accept)+" "+Appconstants.shedulerowdata.getOrderID() +" ?")
		.setIcon(R.drawable.ic_launcher)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	 if(MyscheduleDetail.this.isConnectingToInternet())
				 {
		         new HubbyWsWrapper(MyscheduleDetail.this).new CallRecommendGet(WEB_CALLID.Acceptorder.getTypeCode(), true).execute(Appconstants.userAcceptOrder+"&token="+Tokan+"&orderID="+orderID+"&accept=1");
		 }else
			 DialogCaller.showDialog(MyscheduleDetail.this,MyscheduleDetail.this.getResources().getString(R.string.str_connection));
		 
		 }})
		 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		}).show();
	}
	
	
	private void RejectOrder(final String Tokan,final String orderID)
	{
		new AlertDialog.Builder(MyscheduleDetail.this)
		.setTitle(MyscheduleDetail.this.getResources().getString(R.string.str_huby) )
		.setMessage(MyscheduleDetail.this.getResources().getString(R.string.str_conf_reject)+" "+Appconstants.shedulerowdata.getOrderID() +" ?")
		.setIcon(R.drawable.ic_launcher)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	 if(MyscheduleDetail.this.isConnectingToInternet())
				 {
		         new HubbyWsWrapper(MyscheduleDetail.this).new CallRecommendGet(WEB_CALLID.Acceptorder.getTypeCode(), true).execute(Appconstants.userAcceptOrder+"&token="+Tokan+"&orderID="+orderID+"&accept=0");
		 }else
			 DialogCaller.showDialog(MyscheduleDetail.this,MyscheduleDetail.this.getResources().getString(R.string.str_connection));
		 
		 }})
		 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		}).show();
	}
	
	
	
	

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		
		try
		{
		
		if(ojects.getErrorStatus().equalsIgnoreCase("0"))
		{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyscheduleDetail.this);
				// set title
				alertDialogBuilder.setTitle("Huby");
				// set dialog message
				alertDialogBuilder
					.setMessage(ojects.getMessage())
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							startActivity(new Intent(MyscheduleDetail.this,MySchedule.class));
						}
					  });
					
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();
		}
		}catch(Exception e)
		{
			DialogCaller.showDialog(MyscheduleDetail.this,ojects.getMessage());
		}
		
	}
	
	
	
	
//0 UNKNOWN
//1 GIVE UP
//2 WAITING TOO LONG
//3 DRIVER UNAVAILABLE
//	private void RejectedDialog()
//	{
//		
//		final Dialog dialog = new Dialog(MyscheduleDetail.this);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setCanceledOnTouchOutside(false);
//		dialog.setCancelable(true);
//		dialog.setContentView(R.layout.dialog_rejectorder);
//		
//		 WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//		    lp.copyFrom(dialog.getWindow().getAttributes());
//		    lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//		    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//		    
//		    dialog.getWindow().setAttributes(lp);
//		  
//		//dialog.setTitle("Title...");
//
//		// set the custom dialog components - text, image and button
//		  final EditText edt_addmessage=(EditText)dialog.findViewById(R.id.edt_addmessage);
//
//		Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
//		RadioGroup rdiogroup=(RadioGroup)dialog.findViewById(R.id.rdiogroup);
//		// if button is clicked, close the custom dialog
//		
//		rdiogroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				// TODO Auto-generated method stub
//				 RadioButton rb = (RadioButton) group.findViewById(checkedId);
//				 
//				 if(null!=rb && checkedId > -1){
//	                    if(rb.getText().toString().equalsIgnoreCase("other"))
//	                    {
//	                    	reasonid=0;
//	                    	edt_addmessage.setVisibility(View.VISIBLE);
//	                    }
//	                    else if(rb.getText().toString().equalsIgnoreCase(MyscheduleDetail.this.getResources().getString(R.string.str_give_up)))
//	                    {
//	                    	reasonid=1;
//	                    	edt_addmessage.setVisibility(View.GONE);
//	                    }
//	                    
//	                    else if(rb.getText().toString().equalsIgnoreCase(MyscheduleDetail.this.getResources().getString(R.string.str_waiting)))
//	                    {
//	                    	reasonid=2;
//	                    	edt_addmessage.setVisibility(View.GONE);
//	                    }
//	                    
//	                    else if(rb.getText().toString().equalsIgnoreCase(MyscheduleDetail.this.getResources().getString(R.string.str_driver_un)))
//	                    {
//	                    	reasonid=3;
//	                    	edt_addmessage.setVisibility(View.GONE);
//	                    }
//	                }
//			}
//		});
//		
//		btn_submit.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				
//				CancelOrder(hubySharedprefrence.getLoginsession().getToken(),Appconstants.shedulerowdata.getOrderID(),String.valueOf(reasonid));
//				dialog.dismiss();
//			}
//		});
//
//		dialog.show();
//		
//	  }
//	
	
	
	
	

	/**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
        	
        	
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.fram_map)).getMap();
            
            locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
         
            
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            googleMap.getUiSettings().setAllGesturesEnabled(true);
            googleMap.setTrafficEnabled(true);
            
            
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
           
            
            fromPosition = new LatLng(11.663837, 78.147297);
            toPosition = new LatLng(11.723512, 78.466287);
            
             opts=new PolygonOptions();
             
             ArrayList<LatLng> list = new ArrayList<LatLng>();
             list.add(fromPosition);
             list.add(toPosition);
             

            for (LatLng location : list) {
              opts.add(location);
            }

            
           // GetRouteTask getRoute = new GetRouteTask();
           // getRoute.execute();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
    
    
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
		 LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
		    googleMap.animateCamera(cameraUpdate);
		    locationManager.removeUpdates(this);
		    
		     try
		     {
		     String[] sourcelatlong = Appconstants.shedulerowdata.getSourceLatLng().split(",");
		     double slatitude = Double.parseDouble(sourcelatlong[0]);
		     double slongitude = Double.parseDouble(sourcelatlong[1]);
		     
		     String[] destilatlong = Appconstants.shedulerowdata.getDestinationLatLng().split(",");
		     double dlatitude = Double.parseDouble(destilatlong[0]);
		     double dlongitude = Double.parseDouble(destilatlong[1]);
		     
		        LatLng origin = new LatLng(slatitude,slongitude);
				LatLng dest = new LatLng(dlatitude,dlongitude);
		     
		     cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(slatitude,slongitude), 12);
		     googleMap.animateCamera(cameraUpdate);
		     
		     MarkerOptions marker = new MarkerOptions().position(origin);
			    googleMap.addMarker(marker);
			    
			    MarkerOptions marker1 = new MarkerOptions().position(dest);
			    googleMap.addMarker(marker1);
		     
		     
				 route(origin,dest);
	         } catch (Exception e) {
						// TODO: handle exception
					}

	// Get back the mutable Polyline
	//Polyline polyline = googleMap.addPolyline(rectOptions);
		
	}
	
	
	public void route(LatLng origin,LatLng dest)
	{
		
		// Getting URL to the Google Directions API
		String url = getDirectionsUrl(origin, dest);				
		
		DownloadTask downloadTask = new DownloadTask();
		
		// Start downloading json data from Google Directions API
		downloadTask.execute(url);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	
//	private void CancelOrder(String Tokan,String Orderid,String Reasonid)
//	{
//		 if(isConnectingToInternet())
//		 {
//		new HubbyWsWrapper(MyscheduleDetail.this).new CallRecommendGet(WEB_CALLID.CancelOrder.getTypeCode(), true).execute(Appconstants.userCancelOrder+"&token="+Tokan+"&orderID="+Orderid+"&reasonID="+Reasonid);
//		 }else
//			 DialogCaller.showDialog(MyscheduleDetail.this,MyscheduleDetail.this.getResources().getString(R.string.str_connection));
//	 }
	
	

	
	
	
	/////////////////map By dj/////////////////////
	
	// Fetches data from url passed
		private class DownloadTask extends AsyncTask<String, Void, String>{			
					
			// Downloading data in non-ui thread
			@Override
			protected String doInBackground(String... url) {
					
				// For storing data from web service
				String data = "";
						
				try{
					// Fetching the data from web service
					data = downloadUrl(url[0]);
				}catch(Exception e){
					Log.d("Background Task",e.toString());
				}
				return data;		
			}
			
			// Executes in UI thread, after the execution of
			// doInBackground()
			@Override
			protected void onPostExecute(String result) {			
				super.onPostExecute(result);			
				
				ParserTask parserTask = new ParserTask();
				
				// Invokes the thread for parsing the JSON data
				parserTask.execute(result);
					
			}		
		}
		
		/** A method to download json data from url */
	    private String downloadUrl(String strUrl) throws IOException{
	        String data = "";
	        InputStream iStream = null;
	        HttpURLConnection urlConnection = null;
	        try{
	                URL url = new URL(strUrl);

	                // Creating an http connection to communicate with url 
	                urlConnection = (HttpURLConnection) url.openConnection();

	                // Connecting to url 
	                urlConnection.connect();

	                // Reading data from url 
	                iStream = urlConnection.getInputStream();

	                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

	                StringBuffer sb  = new StringBuffer();

	                String line = "";
	                while( ( line = br.readLine())  != null){
	                        sb.append(line);
	                }
	                
	                data = sb.toString();

	                br.close();

	        }catch(Exception e){
	                Log.d("Exception while downloading url", e.toString());
	        }finally{
	                iStream.close();
	                urlConnection.disconnect();
	        }
	        return data;
	     }
	    
	    
		/** A class to parse the Google Places in JSON format */
	    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
	    	
	    	// Parsing the data in non-ui thread    	
			@Override
			protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
				
				JSONObject jObject;	
				List<List<HashMap<String, String>>> routes = null;			           
	            
	            try{
	            	jObject = new JSONObject(jsonData[0]);
	            	DirectionsJSONParser parser = new DirectionsJSONParser();
	            	
	            	// Starts parsing data
	            	routes = parser.parse(jObject);    
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	            return routes;
			}
			
			// Executes in UI thread, after the parsing process
			@Override
			protected void onPostExecute(List<List<HashMap<String, String>>> result) {
				ArrayList<LatLng> points = null;
				PolylineOptions lineOptions = null;
				MarkerOptions markerOptions = new MarkerOptions();
				
				// Traversing through all the routes
				for(int i=0;i<result.size();i++){
					points = new ArrayList<LatLng>();
					lineOptions = new PolylineOptions();
					
					// Fetching i-th route
					List<HashMap<String, String>> path = result.get(i);
					
					// Fetching all the points in i-th route
					for(int j=0;j<path.size();j++){
						HashMap<String,String> point = path.get(j);					
						
						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);	
						
						points.add(position);						
					}
					
					// Adding all the points in the route to LineOptions
					lineOptions.addAll(points);
					lineOptions.width(5);
					lineOptions.color(Color.RED);	
					
				}
				
				// Drawing polyline in the Google Map for the i-th route
				googleMap.addPolyline(lineOptions);							
			}			
	    }   
	    
	    
	    private String getDirectionsUrl(LatLng origin,LatLng dest){
			
			// Origin of route
			String str_origin = "origin="+origin.latitude+","+origin.longitude;
			
			// Destination of route
			String str_dest = "destination="+dest.latitude+","+dest.longitude;		
			
						
			// Sensor enabled
			String sensor = "sensor=false";			
						
			// Building the parameters to the web service
			String parameters = str_origin+"&"+str_dest+"&"+sensor;
						
			// Output format
			String output = "json";
			
			// Building the url to the web service
			String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
			
			return url;
		}
	    
	    
	    @Override
	    public void onBackPressed() {
	    	// TODO Auto-generated method stub
	    	super.onBackPressed();
	    }
	    
	
	}
	

