package com.vitor.driverapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class OrderCheckoutcomplate extends Baseactivity {
	
	ImageView img_compuncomplate;
	TextView txt_compuncomp;
	Button btn_done;
	int iscomplate=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_checkoutcomplate);
		setActiobarTitle(this.getResources().getString(R.string.title_activity_OrderCheckoutcomplate));
		
		if( getIntent().getExtras() != null)
		{
		  //do here
			Intent intent=getIntent();
			iscomplate=intent.getIntExtra("Transection",0);
		}
		
		  Init();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.chekout_order_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void Init()
	{
		try
		  {
			 img_compuncomplate=(ImageView)findViewById(R.id.img_compuncomplate);
			 txt_compuncomp=(TextView)findViewById(R.id.txt_compuncomp);
			 btn_done=(Button)findViewById(R.id.btn_done); 
			 			 
			 if(iscomplate==0)
			 {
				 txt_compuncomp.setText(this.getResources().getString(R.string.str_ordercomplated));
				 img_compuncomplate.setImageResource(R.drawable.done);
			 }else
			 {
				 txt_compuncomp.setText(this.getResources().getString(R.string.str_orderincomplate));
				 img_compuncomplate.setImageResource(R.drawable.pending);
			 }
		  
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
		}
		
		
		btn_done.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 if(iscomplate==0)
				 {
					 startActivity(new Intent(OrderCheckoutcomplate.this,Homepage.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					 OrderCheckoutcomplate.this.finish();
					
				 }else
				 {
					 OrderCheckoutcomplate.this.finish();
				 }
			}
		});
 }
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		 if(iscomplate==0)
		 {
			 startActivity(new Intent(OrderCheckoutcomplate.this,CheckOut.class));
			 OrderCheckoutcomplate.this.finish();
		 }else
		 {
			 OrderCheckoutcomplate.this.finish();
		 }
	}
	
	
}
