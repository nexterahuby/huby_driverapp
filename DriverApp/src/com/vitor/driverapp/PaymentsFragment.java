package com.vitor.driverapp;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.vitor.driverapp.adapter.ScheduleAdapter;
import com.vitor.driverapp.services.Data;

public class PaymentsFragment extends Fragment {
	
	ListView orderlist;
	ScheduleAdapter mScheduleAdapter;
	String from;
	ArrayList<Data> data;
	ArrayList<Data> paymentsddata;
	LinearLayout lv_norecordfound;
	
	public PaymentsFragment(String from,ArrayList<Data> data)
	{
		this.from=from;
		this.data=data;
	}
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_payments, container, false);
        lv_norecordfound=(LinearLayout)rootView.findViewById(R.id.lv_norecordfound);
        orderlist=(ListView)rootView.findViewById(R.id.orderlist);
        
        Init();
         
        return rootView;
    }
    
    private void Init()
    {   	
    	paymentsddata=new ArrayList<Data>();
     	for(int i=0;i<data.size();i++)
     	{
     		if(data.get(i).getOrderStatusID().equalsIgnoreCase("2"))
     		{
     			paymentsddata.add(data.get(i));
     		}
     	}
    	mScheduleAdapter=new ScheduleAdapter(getActivity(),from,paymentsddata);
    	orderlist.setAdapter(mScheduleAdapter);
    	
    	 if(paymentsddata.size()<=0)
		 {
			 lv_norecordfound.setVisibility(View.VISIBLE);
		 }else
		 {
			 lv_norecordfound.setVisibility(View.GONE); 
		 }
    	
    }
}