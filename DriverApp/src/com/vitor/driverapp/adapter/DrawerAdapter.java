package com.vitor.driverapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vitor.driverapp.R;

@SuppressLint("ResourceAsColor")
public class DrawerAdapter extends BaseAdapter {

	Holder holder;
	Context mContext;
	
	int[] icons={R.drawable.ic_hubby,R.drawable.ic_user,R.drawable.ic_changepass,R.drawable.ic_about,R.drawable.ic_term,R.drawable.ic_rate,R.drawable.ic_feedback,R.drawable.ic_promot,R.drawable.ic_logout};
	String[] name ;	
	
	//={"Home","Edit Profile","Change Password","About us","Privacy and Terms","Rate in store","Send us feed back","Promot App", "Logout"};
    
	public DrawerAdapter(Context mContext) {
		this.mContext = mContext;
		// TODO Auto-generated constructor stub
		name=mContext.getResources().getStringArray(R.array.menulist);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return icons.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int id) {

		// TODO Auto-generated method stub
		return id;
	}

	public static class Holder {
		
		ImageView img_drawer;
		TextView txt_drawer;
		LinearLayout draweritem;

	}

	@Override
	public View getView(int position, View view, ViewGroup vGroup)

	{
		if (view == null) {
			holder = new Holder();
			view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.row_drawer, null);
			
			holder.img_drawer=(ImageView)view.findViewById(R.id.img_drawer);
			holder.txt_drawer=(TextView)view.findViewById(R.id.txt_drawer);
			holder.draweritem=(LinearLayout)view.findViewById(R.id.draweritem);
			
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		final int pos = position;
		
		if(pos==0)
		{
			holder.img_drawer.setImageResource(icons[pos]);
			holder.txt_drawer.setText(name[pos]);
			holder.draweritem.setBackgroundResource(R.color.white);
			
		}else
		{
		holder.img_drawer.setImageResource(icons[pos]);
		holder.txt_drawer.setText(name[pos]);
		}
		return view;
	}

}