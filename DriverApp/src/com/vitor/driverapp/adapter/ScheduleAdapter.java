package com.vitor.driverapp.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vitor.driverapp.MyscheduleDetail;
import com.vitor.driverapp.OrderDetails;
import com.vitor.driverapp.R;
import com.vitor.driverapp.services.Data;
import com.vitor.driverapp.util.Appconstants;

public class ScheduleAdapter extends BaseAdapter {

	Holder holder;
	Context mContext;
	String type;
	ArrayList<Data> data;

	public ScheduleAdapter(Context mContext,String type,ArrayList<Data> data) {
		this.mContext = mContext;
		this.type=type;
		this.data=data;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int id) {

		// TODO Auto-generated method stub
		return id;
	}

	public static class Holder {
		
		ImageButton imb_arrow;
		ImageView img_client;
		LinearLayout lv_myshedule;
		//txt_orderno
		TextView 
		txt_amount
		,txt_time
		,txt_date
		,txt_source
		,txt_dest
		,txt_km
		,txt_name;
		TextView
		txt_user_name,
		txt_comp_name,
		txt_payment_type,
		txt_estaname;

	}

	@Override
	public View getView(int position, View view, ViewGroup vGroup)

	{
		if (view == null) {
			holder = new Holder();
			view = LayoutInflater.from(vGroup.getContext()).inflate(R.layout.row_myschedule, null);
			
			holder.imb_arrow=(ImageButton)view.findViewById(R.id.imb_arrow);
			holder.lv_myshedule=(LinearLayout)view.findViewById(R.id.lv_myshedule);
			
			//holder.txt_orderno=(TextView)view.findViewById(R.id.txt_orderno);
			holder.txt_amount=(TextView)view.findViewById(R.id.txt_amount);
			holder.txt_time=(TextView)view.findViewById(R.id.txt_time);
			holder.txt_date=(TextView)view.findViewById(R.id.txt_date);
			holder.txt_source=(TextView)view.findViewById(R.id.txt_source);
			holder.txt_dest=(TextView)view.findViewById(R.id.txt_dest);
			holder.txt_km=(TextView)view.findViewById(R.id.txt_km);
			holder.txt_name=(TextView)view.findViewById(R.id.txt_name);
			
			holder.txt_user_name=(TextView)view.findViewById(R.id.txt_user_name);
			holder.txt_comp_name=(TextView)view.findViewById(R.id.txt_comp_name);
			holder.txt_payment_type=(TextView)view.findViewById(R.id.txt_payment_type);
			holder.txt_estaname=(TextView)view.findViewById(R.id.txt_estaname);
			
			holder.img_client=(ImageView)view.findViewById(R.id.img_client);
			
			
			view.setTag(holder);
		} else {
			holder = (Holder) view.getTag();
		}
		final int pos = position;
		final Data rowItem = data.get(pos);
		
		 try
		 {
			
			holder.txt_amount.setText("$ "+rowItem.getCostTotal());
			String[] splited = rowItem.getDatetime().split("\\s+");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format.parse(splited[0]);
			SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = sm.format(date);
			holder.txt_date.setText(strDate);
			holder.txt_time.setText(splited[1]);
			holder.txt_date.setText(strDate);
			holder.txt_source.setText(rowItem.getSource());
			holder.txt_dest.setText(rowItem.getDestination());
			holder.txt_km.setText(rowItem.getOrderStatus());
			holder.txt_name.setText(rowItem.getSource());
			
			holder.txt_payment_type.setText(rowItem.getPayment());
			
			holder.txt_user_name.setText(rowItem.getUser());
			holder.txt_comp_name.setText(rowItem.getOrganization());
			holder.txt_estaname.setText(rowItem.getEstablishment());
			//holder.txt_payment_type.setText("");
			
			if(rowItem.getOrderStatusID().equalsIgnoreCase("3"))
			{
				holder.img_client.setImageResource(R.drawable.bg_circle_red);
			}else if(rowItem.getOrderStatusID().equalsIgnoreCase("0"))
			{
				holder.img_client.setImageResource(R.drawable.bg_circle_yellow);
			}else if(rowItem.getOrderStatusID().equalsIgnoreCase("1"))
			{
				holder.img_client.setImageResource(R.drawable.bg_circle_green);
			}else if(rowItem.getOrderStatusID().equalsIgnoreCase("2"))
			{
				holder.img_client.setImageResource(R.drawable.bg_circle_blue);
			}
			else
			{
				holder.img_client.setImageResource(R.drawable.bg_circle_yellow);
			}
					
			
		 }catch(Exception e)
		 {
			 e.printStackTrace();
			Log.e("eroor", e.getLocalizedMessage());
		 }
		
		
		
		holder.lv_myshedule.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(type.equalsIgnoreCase("schedule"))
				{
					Appconstants.shedulerowdata=rowItem;
				mContext.startActivity(new Intent(mContext,MyscheduleDetail.class));
				}else
				{
					mContext.startActivity(new Intent(mContext,OrderDetails.class));
				}
			}
		});
		
		

		return view;
	}

}
