package com.vitor.driverapp.adapter;

import java.util.ArrayList;

import com.vitor.driverapp.AcceptedFragment;
import com.vitor.driverapp.PaymentsFragment;
import com.vitor.driverapp.PendingFragment;
import com.vitor.driverapp.RejectedFragment;
import com.vitor.driverapp.services.Data;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {
	
	String from;
	ArrayList<Data> data;
 
    public TabsPagerAdapter(FragmentManager fm,String from,ArrayList<Data> data) {
        super(fm);
        this.from=from;
    	this.data=data;
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Top Rated fragment activity
        	 return new PendingFragment(from,data);
         
        case 1:
            // Games fragment activity
        	   return new AcceptedFragment(from,data);
          
        case 2:
            // Movies fragment activity
        	  return new RejectedFragment(from,data);
        	  
        case 3:
            // Movies fragment activity
        	  return new PaymentsFragment(from,data);	  
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}