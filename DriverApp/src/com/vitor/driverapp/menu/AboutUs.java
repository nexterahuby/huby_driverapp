package com.vitor.driverapp.menu;

import com.vitor.driverapp.BaseContainerFragment;
import com.vitor.driverapp.R;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


@SuppressWarnings("deprecation")
public class AboutUs extends BaseContainerFragment {
	
	TextView txt_aboutus;
	ActionBarActivity activity;

	public AboutUs(ActionBarActivity activity) {
		this.activity=activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_about_us, container,false);
		txt_aboutus=(TextView)v.findViewById(R.id.txt_aboutus);
		
		 activity.getSupportActionBar().setCustomView(R.layout.actionbarwithlefticon); 
	       View view = activity.getSupportActionBar().getCustomView();
	       ImageView imgicon=(ImageView)view.findViewById(R.id.lefticon);
	       TextView titleTxtView = (TextView) view.findViewById(R.id.mytext);
	       titleTxtView.setText(this.getResources().getString(R.string.str_aboutus));
	       imgicon.setImageResource(R.drawable.ic_about);
		
	       txt_aboutus.setText(Html
					.fromHtml("<p>Desenvolvido pela NEXTERA SERVIÇOS em 2010 como solução de gerenciamento de despesas corporativas, o <b>HUBY</b> é atualmente utilizado por inúmeros estabelecimentos como uma plataforma capaz de atender o mercado corporativo de transportes, otimizando processos internos, integrando com excelência a tecnologia e informação.</p>"));
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}
	
//	@Override
//	public void onDetach() {
//		// TODO Auto-generated method stub
//		super.onDetach();
//		Homepage.isfinish=false;
//	}

}
