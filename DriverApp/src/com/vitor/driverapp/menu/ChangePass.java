package com.vitor.driverapp.menu;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.vitor.driverapp.BaseContainerFragment;
import com.vitor.driverapp.Homepage;
import com.vitor.driverapp.R;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

@SuppressWarnings("deprecation")
public class ChangePass extends BaseContainerFragment implements WSResponseListener{
	
	
	EditText edt_password;
	EditText edt_confirmpass;
	Button btn_login;
	HubySharedprefrence mHubySharedprefrence;
	ActionBarActivity activity;

	public ChangePass(ActionBarActivity activity) {
		
		this.activity=activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_change_pass, container,false);
		edt_password=(EditText)v.findViewById(R.id.edt_password);
		edt_confirmpass=(EditText)v.findViewById(R.id.edt_confirmpass);
		btn_login=(Button)v.findViewById(R.id.btn_login);
		
		   activity.getSupportActionBar().setCustomView(R.layout.actionbarwithlefticon); 
	       View view = activity.getSupportActionBar().getCustomView();
	       TextView titleTxtView = (TextView) view.findViewById(R.id.mytext);
	       ImageView imgicon=(ImageView)view.findViewById(R.id.lefticon);
	       titleTxtView.setText(this.getResources().getString(R.string.str_changepass));
	       imgicon.setImageResource(R.drawable.ic_changepass);
	       
	       Init();
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}
	
	private void Init()
	{
		mHubySharedprefrence=new HubySharedprefrence(activity);
		btn_login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Validation())
				{
					Resetpass(mHubySharedprefrence.getLoginsession().getToken(),edt_confirmpass.getText().toString());
				}
			}
		});
		
	}
	
	private Boolean Validation()
	{
		
		 if(edt_password.getText().toString().equals("") || edt_password.getText().toString().equals(null))
		{
			edt_password.setError(this.getResources().getString(R.string.str_req_pass));
			edt_password.requestFocus();
			return false;
		}
		 
		 else if(edt_confirmpass.getText().toString().equals("") || edt_confirmpass.getText().toString().equals(null))
		{
			edt_confirmpass.setError(this.getResources().getString(R.string.str_req_confpass));
			edt_confirmpass.requestFocus();
			return false;
		}
		  if(!edt_confirmpass.getText().toString().equals(edt_password.getText().toString()) )
			{
				edt_confirmpass.setError(this.getResources().getString(R.string.str_req_pass_match));
				edt_confirmpass.requestFocus();
				return false;
			}
		 
		 return true;
	}
	
	
	private void Resetpass(final String Tokan,final String password)
	{
		new AlertDialog.Builder(getActivity())
		.setTitle(getActivity().getResources().getString(R.string.str_huby) )
		.setMessage(getActivity().getResources().getString(R.string.str_conf_changepass))
		.setIcon(R.drawable.ic_launcher)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	 if(((Homepage)getActivity()).isConnectingToInternet())
				 {
		new HubbyWsWrapper(activity,ChangePass.this).new CallRecommendGet(WEB_CALLID.ResetPass.getTypeCode(), true).execute(Appconstants.driverResetPassword+"&token="+Tokan+"&pass="+password);
		 }else
			 DialogCaller.showDialog(getActivity(),getActivity().getResources().getString(R.string.str_connection));
		 
		 }})
		 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		}).show();
	}

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		
		try
		{
		
		if(ojects.getErrorStatus().equalsIgnoreCase("0"))
		{
			Log.d("changepass",ojects.getMessage());
			DialogCaller.showDialog(this.getActivity(),this.getResources().getString(R.string.str_passchanged));
		}else
		{
			DialogCaller.showDialog(this.getActivity(),ojects.getMessage());
		}
		
		}catch(Exception e)
		{
			DialogCaller.showDialog(this.getActivity(),ojects.getMessage());
			e.printStackTrace();
		}
	}

}
