package com.vitor.driverapp.menu;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.vitor.driverapp.BaseContainerFragment;
import com.vitor.driverapp.R;

@SuppressWarnings("deprecation")
public class PrivacyAndTerms extends BaseContainerFragment {
	
	WebView web_privacy;
	ActionBarActivity activity;

	public PrivacyAndTerms(ActionBarActivity activity) {

		this.activity=activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_privacy, container,false);
		
		web_privacy=(WebView)v.findViewById(R.id.web_privacy);
		
		   activity.getSupportActionBar().setCustomView(R.layout.actionbarwithlefticon); 
	       View view = activity.getSupportActionBar().getCustomView();
	       ImageView imgicon=(ImageView)view.findViewById(R.id.lefticon);
	       TextView titleTxtView = (TextView) view.findViewById(R.id.mytext);
	       titleTxtView.setText(this.getResources().getString(R.string.str_privacyandterm));
	       imgicon.setImageResource(R.drawable.ic_term);

		
       web_privacy.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	       
	       loadLocalFile("file:///android_asset/terms.html");

		return v;
		
	}
	
	private void loadLocalFile(String Url) {
		web_privacy.loadUrl(Url);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}
}