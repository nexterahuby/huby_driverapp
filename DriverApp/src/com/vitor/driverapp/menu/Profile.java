package com.vitor.driverapp.menu;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.vitor.driverapp.BaseContainerFragment;
import com.vitor.driverapp.Homepage;
import com.vitor.driverapp.R;
import com.vitor.driverapp.services.HubbyWsWrapper;
import com.vitor.driverapp.services.HubbyWsWrapper.WEB_CALLID;
import com.vitor.driverapp.services.HubbyWsWrapper.WSResponseListener;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.Hubypost;
import com.vitor.driverapp.util.Appconstants;
import com.vitor.driverapp.util.CircleTransform;
import com.vitor.driverapp.util.DialogCaller;
import com.vitor.driverapp.util.HubySharedprefrence;

public class Profile extends BaseContainerFragment implements WSResponseListener{
	
	EditText edt_name,edt_email;
	HubySharedprefrence hubySharedprefrence;
	Button btn_update;
	ImageView profile_img;
	private static int RESULT_LOAD_IMAGE = 1;
	

	public Profile() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((Homepage)getActivity()).setActionbartitleWithlefticon(this.getResources().getString(R.string.str_editprofile),R.drawable.ic_user);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_profile, container,false);
		hubySharedprefrence=new HubySharedprefrence(getActivity());
		edt_name=(EditText)v.findViewById(R.id.edt_name);
		edt_email=(EditText)v.findViewById(R.id.edt_email);
		btn_update=(Button)v.findViewById(R.id.btn_update);
		profile_img=(ImageView)v.findViewById(R.id.profile_img);
		
		edt_name.setText(hubySharedprefrence.getLoginsession().getName());
		edt_email.setText(hubySharedprefrence.getLoginsession().getEmail());
	
		
		btn_update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Validation())
				{
					Resetpass(hubySharedprefrence.getLoginsession().getToken(),edt_name.getText().toString(),edt_email.getText().toString());
				}
			}
		});
		
		profile_img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		          Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	              startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});

		return v;
	}
	
	@SuppressWarnings("deprecation")
	private void Resetpass(final String Tokan, final String name,final String email)
	{
		new AlertDialog.Builder(getActivity())
		.setTitle(getActivity().getResources().getString(R.string.str_huby) )
		.setMessage(getActivity().getResources().getString(R.string.str_conf_editpf))
		.setIcon(R.drawable.ic_launcher)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		    public void onClick(DialogInterface dialog, int whichButton) {
		 if(((Homepage)getActivity()).isConnectingToInternet())
		 {
			 
			 Hubypost hubypost = null;
			 try
			 {
			 profile_img.setDrawingCacheEnabled(true);
			 profile_img.buildDrawingCache();
			 Bitmap bm = profile_img.getDrawingCache();
			 ByteArrayOutputStream stream = new ByteArrayOutputStream();
			 bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
			 byte[] byteArrayImage=stream.toByteArray();
			 
			 String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
			 
			 hubypost =new Hubypost();
			 hubypost.setPicture(encodedImage);
			 }catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			 
		new HubbyWsWrapper(getActivity(),Profile.this).new CallRecommendGet(WEB_CALLID.Editprofile.getTypeCode(), true).execute(Appconstants.userUpdateProfile+"&token="+Tokan+"&name="+URLEncoder.encode(name)+"&email="+URLEncoder.encode(email),hubypost);
		 }else
		 {
			 DialogCaller.showDialog(getActivity(),getActivity().getResources().getString(R.string.str_connection));
		 }
		 
		    }})
			 .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			}).show();
		 
		 }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}
	
	private Boolean Validation()
	{
		if(edt_name.getText().toString().equals("") || edt_name.getText().toString().equals(null))
		{
			edt_name.setError(this.getResources().getString(R.string.str_req_name));
			edt_name.requestFocus();
			return false;
		}
		else if(edt_email.getText().toString().equals("") || edt_email.getText().toString().equals(null))
		{
			edt_email.setError(this.getResources().getString(R.string.str_req_email));
			edt_email.requestFocus();
			return false;
		}
		
		return true;
	}

	@Override
	public void onPostResponse(Object object, int callId) {
		// TODO Auto-generated method stub
		HubyDTO ojects = (HubyDTO) object;
		try
		{
		if(ojects.getErrorStatus().equalsIgnoreCase("0"))
		{
		DialogCaller.showDialog(getActivity(),this.getResources().getString(R.string.str_updateprofile));
		hubySharedprefrence.setEmail(edt_email.getText().toString());
		hubySharedprefrence.setName(edt_name.getText().toString());
		
		btn_update.requestFocus();
		}else
		{
			DialogCaller.showDialog(getActivity(),ojects.getMessage());
		}
		}catch(Exception e)
		{
			DialogCaller.showDialog(getActivity(),ojects.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		 if (requestCode == RESULT_LOAD_IMAGE && resultCode == getActivity().RESULT_OK && null != data) {
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = { MediaStore.Images.Media.DATA };
	 
	            Cursor cursor = getActivity().getContentResolver().query(selectedImage,filePathColumn, null, null, null);
	            cursor.moveToFirst();
	 
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            String picturePath = cursor.getString(columnIndex);
	            cursor.close();
	            
	            Picasso.with(getActivity()).load(selectedImage).transform(new CircleTransform()).into(profile_img);
	             
	           
	            //profile_img.setImageBitmap(BitmapFactory.decodeFile(picturePath));
	         
	        }
	}
}