package com.vitor.driverapp.services;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

	private String orderID;
	private String orderStatusID;
	private String orderStatus;
	private String driverID;
	private String establishmentID;
	private String establishment;
	private String organization;
	private String user;
	private String driver;
	private String driverPhone;
	private String datetime;
	private String source;
	private String destination;
	private String carPlate;
	private String carManufacturer;
	private String carModel;
	private String carColor;
	private String instructions;
	private String km;
	private String costToll;
	private String costParking;
	private String costWaiting;
	private String costTotal;
	private String sourceLatLng;
	private String destinationLatLng;
	private String payment;
	
  

	@JsonProperty("orderID")
	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	
	@JsonProperty("orderStatusID")
	public String getOrderStatusID() {
		return orderStatusID;
	}
	
	public void setOrderStatusID(String orderStatusID) {
		this.orderStatusID = orderStatusID;
	}
	@JsonProperty("orderStatus")
	public String getOrderStatus() {
		return orderStatus;
	}
	
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	@JsonProperty("driverID")
	public String getDriverID() {
		return driverID;
	}

	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	@JsonProperty("establishmentID")
	public String getEstablishmentID() {
		return establishmentID;
	}

	public void setEstablishmentID(String establishmentID) {
		this.establishmentID = establishmentID;
	}
	@JsonProperty("establishment")
	public String getEstablishment() {
		return establishment;
	}

	public void setEstablishment(String establishment) {
		this.establishment = establishment;
	}
	@JsonProperty("organization")
	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	@JsonProperty("user")
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	@JsonProperty("driver")
	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
	@JsonProperty("driverPhone")
	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}
	@JsonProperty("datetime")
	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	@JsonProperty("destination")
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	@JsonProperty("carPlate")
	public String getCarPlate() {
		return carPlate;
	}

	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	@JsonProperty("carManufacturer")
	public String getCarManufacturer() {
		return carManufacturer;
	}

	public void setCarManufacturer(String carManufacturer) {
		this.carManufacturer = carManufacturer;
	}
	@JsonProperty("carModel")
	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	@JsonProperty("carColor")
	public String getCarColor() {
		return carColor;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}
	@JsonProperty("instructions")
	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	@JsonProperty("km")
	public String getKm() {
		return km;
	}

	public void setKm(String km) {
		this.km = km;
	}
	@JsonProperty("costToll")
	public String getCostToll() {
		return costToll;
	}

	public void setCostToll(String costToll) {
		this.costToll = costToll;
	}
	@JsonProperty("costParking")
	public String getCostParking() {
		return costParking;
	}

	public void setCostParking(String costParking) {
		this.costParking = costParking;
	}
	
	@JsonProperty("costWaiting")
	public String getCostWaiting() {
		return costWaiting;
	}

	public void setCostWaiting(String costWaiting) {
		this.costWaiting = costWaiting;
	}
	@JsonProperty("costTotal")
	public String getCostTotal() {
		return costTotal;
	}

	public void setCostTotal(String costTotal) {
		this.costTotal = costTotal;
	}
	
	@JsonProperty("sourceLatLng")
	public String getSourceLatLng() {
		return sourceLatLng;
	}

	public void setSourceLatLng(String sourceLatLng) {
		this.sourceLatLng = sourceLatLng;
	}
	
	@JsonProperty("destinationLatLng")
	public String getDestinationLatLng() {
		return destinationLatLng;
	}

	public void setDestinationLatLng(String destinationLatLng) {
		this.destinationLatLng = destinationLatLng;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}
	
	

}
