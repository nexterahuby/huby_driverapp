package com.vitor.driverapp.services;

import com.vitor.driverapp.util.HubyWS;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;


public class HubbyWsWrapper {
	
	private final int Call_DriverLogin=1,Call_ResetPass=2,Call_OrderList=3,Call_checkoutorder=4,Call_editProfile=5,Call_CancelOrder=6,Call_Pinglocation=7
			,Call_Rateorder=8,Call_Driver_updatestatus=9,Call_Forgotpass=11,Call_Acceptorder=12,Call_updateDriver_invite=13,Call_driverOrdersPendingCheckout=14;
	
	private WSResponseListener wsResponseListener;
	public Context context;
	
	public HubbyWsWrapper(Context context) 
	{
		this.context = context;
		wsResponseListener = (WSResponseListener) context;
	}
	
	public HubbyWsWrapper(Context context, Fragment fragment) {
		
		this.context = context;
		wsResponseListener = (WSResponseListener) fragment;
	}
	
	
	public interface WSResponseListener {
		
		public void onPostResponse(Object object, int callId);

//		public void saveAllCoupons(SignInDTO signInDTO,
//				RecommendPOST recommendPost, int callId);
	}
	
	
	public enum WEB_CALLID {
		         DriverLogin(1),ResetPass(2),OrderList(3),CheckOutorder(4),Editprofile(5),CancelOrder(6),Pinglocation(7)
		         ,RateOrder(8),Driver_updatestatus(9),Forgotpass(11),Acceptorder(12),UpdateDriverInvite(13),driverOrdersPendingCheckout(14);

		int callId;

		private WEB_CALLID(int s) {
			callId = s;
		}

		public final int getTypeCode() {
			return callId;
		}
	}
	
	

public class CallRecommendGet extends AsyncTask<Object, Void, Object> {

	int callId;
	boolean displayProgress;
	ProgressDialog progressDialog;

	public CallRecommendGet(int callId, boolean displayProgress) {
		this.callId = callId;
		this.displayProgress = displayProgress;

	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		if (displayProgress) {
			progressDialog = new ProgressDialog(context);
			progressDialog.setMessage("Aguarde...");
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.show();

		}
	}

	@Override
	protected Object doInBackground(Object... params) {

		switch (callId) {
			
		case Call_DriverLogin:
		
			try {
				return new Hubyproxy().Driverlogin(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_ResetPass:
			
			try {
				return new Hubyproxy().ResettPass(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_OrderList:
			
			try {
				return new Hubyproxy().GetOrderList(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_checkoutorder:
			
			try {
				return new Hubyproxy().GetOrderList(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_editProfile:
			
			
			try {
				return new Hubyproxy().UpdateProfile(HubyWS.Huby_WS_URL+params[0],(Hubypost)params[1]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_CancelOrder:
			
			try {
				return new Hubyproxy().CancelOrder(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_Pinglocation:
			
			try {
				return new Hubyproxy().Pinglocation(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_Rateorder:
			
			try {
				return new Hubyproxy().RateOrder(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_Driver_updatestatus:
			
			try {
				return new Hubyproxy().DriverUpdate(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_Forgotpass:
			
			try {
				return new Hubyproxy().Forgotpass(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_Acceptorder:
			

			try {
				return new Hubyproxy().Acceptorder(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_updateDriver_invite:
			
			try {
				return new Hubyproxy().UpdateDriverInvite(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		case Call_driverOrdersPendingCheckout:
			
			try {
				return new Hubyproxy().GetdriverOrdersPendingCheckout(HubyWS.Huby_WS_URL+params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
		default:
			break;
		}
		return null;
	}

	@Override
	protected void onPostExecute(Object result) {

		super.onPostExecute(result);
		if (displayProgress) {
			progressDialog.dismiss();
		}
		wsResponseListener.onPostResponse(result, callId);

	}

}



}
