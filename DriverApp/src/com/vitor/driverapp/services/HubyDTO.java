package com.vitor.driverapp.services;

import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude( Include.NON_NULL )
@JsonIgnoreProperties( ignoreUnknown = true )
public class HubyDTO {

	private String organizationID;
	private String userID;
	private String organization;
	private String name;
	private String email;
	private String phone;
	private String transactions;
	private String balance;
	private String appointments;
	private String token;
	private String status="";
	private String message;
	private String errorStatus;
	private String driverID;
	
	private ArrayList<Data> items;
	private ArrayList<Data> invites;
	
	/*private String orderID;
	private String user;
	private String source;
	private String destination;
	private String sourceLatLng;
	private String destinationLatLng;*/
	



	@JsonProperty("organizationID")
	public String getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}
	@JsonProperty("userID")
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
	@JsonProperty("organization")
	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	@JsonProperty("transactions")
	public String getTransactions() {
		return transactions;
	}

	public void setTransactions(String transactions) {
		this.transactions = transactions;
	}
	@JsonProperty("balance")
	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	@JsonProperty("appointments")
	public String getAppointments() {
		return appointments;
	}

	public void setAppointments(String appointments) {
		this.appointments = appointments;
	}
	@JsonProperty("token")
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@JsonProperty("errorStatus")
	public String getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(String errorStatus) {
		this.errorStatus = errorStatus;
	}

	@JsonProperty("items")
	public ArrayList<Data> getItems() {
		return items;
	}

	public void setItems(ArrayList<Data> items) {
		this.items = items;
	}

	@JsonProperty("invites")
	public ArrayList<Data> getInvites() {
		return invites;
	}

	public void setInvites(ArrayList<Data> invites) {
		this.invites = invites;
	}

	public String getDriverID() {
		return driverID;
	}

	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	
	
	

	/*public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSourceLatLng() {
		return sourceLatLng;
	}

	public void setSourceLatLng(String sourceLatLng) {
		this.sourceLatLng = sourceLatLng;
	}

	public String getDestinationLatLng() {
		return destinationLatLng;
	}

	public void setDestinationLatLng(String destinationLatLng) {
		this.destinationLatLng = destinationLatLng;
	}
	*/

}
