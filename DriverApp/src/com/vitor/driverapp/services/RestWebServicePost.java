package com.vitor.driverapp.services;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;


@SuppressWarnings("deprecation")
public class RestWebServicePost {

    private static final String LOG_TAG = "RestWebServicePost";
    
    private static ObjectMapper mapper = null;// new ObjectMapper();

    private static final Lock lock = new ReentrantLock();
    HttpPost httpPost;
    private String url,filePath="",userId="";
    private HttpClient client;
    private HttpResponse response;
    
    public RestWebServicePost( String url ) 
    {
        this.url = url;
        client = new DefaultHttpClient();
    }
    
    public RestWebServicePost( String url,String filePath,String userId ) 
    {
        this.url = url;
        this.filePath = filePath;
        this.userId = userId;
        client = new DefaultHttpClient();
    }

    public <Request, Response> Response execute( Request request, Class<Response> responseType )
    {
    	Response ret = null;
        int statusCode = 0;
        try
        {
            if ( Log.isLoggable( LOG_TAG, Log.DEBUG ) )
            {
                Log.d( LOG_TAG, "Executing url: " + url ); 
            }
            httpPost = new HttpPost( url );
           // super.request = httpPost;

            // prepare the json request
            ObjectWriter writer = getMapper().writer();

            String jsonObject = "";
            if ( request != null )
            {
                writer.writeValueAsString( request );
                jsonObject = writer.writeValueAsString( request );
            }

            Log.d( LOG_TAG, "JSON request:::" + jsonObject );

            StringEntity entity = new StringEntity( jsonObject,"UTF-8" );

            //entity.setContentType( "application/json" );
            
           // httpPost.setHeader( "Accept", "application/json" );
            
            
            MultipartEntity multipartEntity = null;
          
            	httpPost.setEntity( entity );

            // now send the request
            response = client.execute( httpPost );

            if ( httpPost.isAborted() )
            {
                Log.d( LOG_TAG, "JSON request::: aborted " );
                throw new Exception( String.format( "Operation [%s] is aborted.", url ) );
            }
            statusCode = response.getStatusLine().getStatusCode();
            Log.d( LOG_TAG, "JSON request::: statusCode " + statusCode ); 
            // read the response
            if ( statusCode == 200 )
            {
                if ( responseType != Void.class )
                {
                    HttpEntity httpEntity = response.getEntity();
                    String data = EntityUtils.toString( httpEntity );
                    ret = getMapper().readValue( data, responseType ); 
                }
            }
            else
            {
                ret = null;
            }
            httpPost = null;
        }
        catch ( Exception ex )
        {
            Log.e( LOG_TAG, "Status code: " + Integer.toString( statusCode ) + " Exception thrown: " + ex.getMessage() ); 
            ret = null;
        }

        return ret;
    }

    public void abort()
        throws Exception
    {
        if ( httpPost != null )
        {
            httpPost.abort();
            httpPost = null;
        }
    }
    
    protected synchronized ObjectMapper getMapper()
    {
        if ( mapper != null )
            return mapper;

       try
        {
            lock.lock();
            if ( mapper == null )
            {
                mapper = new ObjectMapper();
            }
            lock.unlock();
        }
        catch ( Exception e )
        {
            Log.e( LOG_TAG, "Mapper initialization failed. Exception: " + e.getMessage() );
        }

        return mapper;
    }

}
