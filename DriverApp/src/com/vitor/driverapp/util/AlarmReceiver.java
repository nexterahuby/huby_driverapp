package com.vitor.driverapp.util;

import java.util.List;
import java.util.Locale;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.vitor.driverapp.Homepage;
import com.vitor.driverapp.R;
import com.vitor.driverapp.services.HubyDTO;
import com.vitor.driverapp.services.Hubyproxy;


public class AlarmReceiver extends  BroadcastReceiver {
	
	Context context;
	Double	latitude; 
	Double	longitude;
	HubySharedprefrence mSharedprefrence;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		
		// Toast.makeText(context, "Heloo", Toast.LENGTH_LONG).show();
		mSharedprefrence=new HubySharedprefrence(context);
		this.context=context;
		
		 try {
				GPSTracker gpsTracker = new GPSTracker(context);
					// check if GPS enabled
					if (gpsTracker.canGetLocation()) 
					{
						latitude = gpsTracker.getLatitude();
						longitude = gpsTracker.getLongitude();
						
						new getzipcode().execute(latitude,longitude);
					} else 
					{
						gpsTracker.showSettingsAlert();
					}
					
		 } catch (Exception e) 
			{
				e.printStackTrace();
			}
	}
	
	public class CallRecommendGet extends AsyncTask<Object, Void, Object> {

		int callId;
		boolean displayProgress;
		ProgressDialog progressDialog;

		public CallRecommendGet(int callId, boolean displayProgress) {
			this.callId = callId;
			this.displayProgress = displayProgress;

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (displayProgress) {
				progressDialog = new ProgressDialog(context);
				progressDialog.setMessage("Processing...");
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.show();

			}
		}

		@Override
		protected Object doInBackground(Object... params) {

				try {
					return new Hubyproxy().Pinglocation(HubyWS.Huby_WS_URL+params[0]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
			return null;
		}

		@Override
		protected void onPostExecute(Object result) {
			
			super.onPostExecute(result);
			if (displayProgress) {
				progressDialog.dismiss();
			}
			if(null!= result)
			{
		    HubyDTO ojects = (HubyDTO) result;
			Log.d("Result of pinglocation",ojects.toString());
			try
			{
			if(null!= ojects.getInvites())
				if(ojects.getInvites().size()>=1)
					
					if(!Helper.ordergetonce.containsKey(ojects.getInvites().get(0).getOrderID()))
					{
						Helper.ordergetonce.put(ojects.getInvites().get(0).getOrderID(),ojects.getInvites().get(0).getOrderID());
						notifyUser(ojects);
						Homepage.customeprogressdialog(context,ojects);
						//notifyUser();
					}
			
			}catch(Exception e){
				e.printStackTrace();
			}
			}

		}
} 
	
	/* private String getAddressFromLocation(Double lat, Double lng) 
		{
			try {
				Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
				List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
				return addresses.get(0).getPostalCode();

			} catch (Exception e) 
			{
				e.printStackTrace();
				return "";
			}
		}*/
		 
	 
	 private class getzipcode extends AsyncTask<Double, Void, Void> {
			
			String Postalcode="";
			
			@Override
			protected Void doInBackground(Double... params) {
				// TODO Auto-generated method stub
				Double lat = params[0];
				Double lng = params[1];
				try {
					Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
					List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
					Postalcode= addresses.get(0).getPostalCode();

				} catch (Exception e) 
				{
					e.printStackTrace();
					Postalcode= "";
				}
				 
				return null;
			}
			
			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				
				Log.e("postalcode","curent postal code is ="+Postalcode);
				//String postalcode =Homepage.getPostalcode(latitude, longitude);
				if(null != Postalcode)
				{
					if(!Postalcode.equalsIgnoreCase("") || !Postalcode.equalsIgnoreCase(" "))
					{
				      mSharedprefrence.setZipcode((Postalcode));
					}
				}
						
				 String zipcode=mSharedprefrence.getZipcode();
				
				 String url=Appconstants.userPingLocation+"&token="+mSharedprefrence.getLoginsession().getToken()+"&latitude="+latitude+"&longitude="+longitude+"&Zipcode="+zipcode+"";
				 Log.e("request string","request string ="+url);
				 //new WebServiceRequestGet(url).execute(HubyDTO.class);
				 new CallRecommendGet(1,false).execute(url);
								
			}
		
		}
	 
	 @SuppressWarnings("deprecation")
	public void notifyUser(HubyDTO ojects){
		 
		    Homepage.fromnotyobject=ojects;
		    NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		 
		    Intent intent = new Intent(context, Homepage.class);
		    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		    intent.putExtra("fromnoti", true);
		    
		     
		    //use the flag FLAG_UPDATE_CURRENT to override any notification already there
		    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		 
		    Notification notification = new Notification(R.drawable.ic_launcher, context.getString(R.string.app_name), System.currentTimeMillis());
		    notification.flags = Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
		 		    		    
		    notification.setLatestEventInfo(context, context.getString(R.string.app_name), context.getString(R.string.str_notificationtext), contentIntent);
		    //10 is a random number I chose to act as the id for this notification
		    notificationManager.notify(10, notification);
		    
		}
	
	 
	
}
