package com.vitor.driverapp.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vitor.driverapp.services.Data;

public class Appconstants {
	
	public static String userOrdersList="driverOrdersList";
	public static String driverOrderCheckout="driverOrderCheckout";
	public static String userUpdateProfile="driverUpdateProfile";
	public static String userCancelOrder="driverUpdateInvite";
	
	public static String userPingLocation="driverPingLocation";
	public static String userRatingOrder="driverOrderRating";
	
	public static String userAcceptOrder="driverUpdateInvite";
	
	public static String driverLogin="driverLogin";
	
	public static String driverResetPassword="driverResetPassword";
	
	public static String driverUpdateStatus="driverUpdateStatus";
	
	public static String driverForgotPassword="driverForgotPassword";
	
	public static String updateDriverInvite="driverUpdateInvite";
	
	public static String driverOrdersPendingCheckout="driverOrdersPendingCheckout";
	
	public static Data rowdata;
	public static Data shedulerowdata;
	
	public static Data invitedata;
	
	
	
	public static boolean emailValidator(final String mailAddress) {

	    Pattern pattern;
	    Matcher matcher;

	    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	    pattern = Pattern.compile(EMAIL_PATTERN);
	    matcher = pattern.matcher(mailAddress);
	    return matcher.matches();

	}
	
	
}
