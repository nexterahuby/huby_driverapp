package com.vitor.driverapp.util;

import com.vitor.driverapp.bean.LoginBean;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class HubySharedprefrence {
	
	SharedPreferences pref;
	Editor editor;
	Context _Context;
	int PRIVATE_MODE = 0;
	
	public static final String Is_newuser = "is_newuser";
	private static final String PREF_NAME = "Login_pref";
	
	public static final String organizationID="organizationID";
	public static final String userID="userID";
	public static final String organization="organization";
	public static final String name="name";
	public static final String email="email";
	public static final String phone="phone";
	public static final String transactions="transactions";
	public static final String balance="balance";
	public static final String appointments="appointments";
	public static final String token="token";
	
	public static final String lastzipcode="zipcode";

	
	public HubySharedprefrence(Context context) 
	{
		this._Context = context;
		pref = _Context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		//editor = pref.edit();
	}
	
	public void setIsnewuser(Boolean isnewuser) {
		PreferenceManager.getDefaultSharedPreferences(_Context).edit()
				.putBoolean(Is_newuser, isnewuser).commit();
	}

	public Boolean getIsnewuser() {
		return PreferenceManager.getDefaultSharedPreferences(_Context)
				.getBoolean(Is_newuser, true);
	}
	
	
	public void setLoginsession(LoginBean hubyDTO)
	{
		Log.d("name",""+hubyDTO.getName());
		
		pref = _Context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		pref.edit().putString(name,hubyDTO.getName()).commit();
		pref.edit().putString(organizationID, hubyDTO.getOrganizationID()).commit();
		pref.edit().putString(userID, hubyDTO.getUserID()).commit();
		pref.edit().putString(organization, hubyDTO.getOrganization()).commit();
		pref.edit().putString(email, hubyDTO.getEmail()).commit();
		pref.edit().putString(phone, hubyDTO.getPhone()).commit();
		pref.edit().putString(transactions, hubyDTO.getTransactions()).commit();
		pref.edit().putString(balance, hubyDTO.getBalance()).commit();
		pref.edit().putString(appointments, hubyDTO.getAppointments()).commit();
		pref.edit().putString(token, hubyDTO.getToken()).commit();
	}
	
	
	
	
	public LoginBean getLoginsession()
	{
		LoginBean hubyDTO =new LoginBean();
		hubyDTO.setName(pref.getString(name,"Dashrath"));
		hubyDTO.setOrganizationID(pref.getString(organizationID, ""));
		hubyDTO.setUserID(pref.getString(userID, "0"));
		hubyDTO.setOrganization(pref.getString(organization, ""));
		hubyDTO.setEmail(pref.getString(email, ""));
		hubyDTO.setPhone(pref.getString(phone, ""));
		hubyDTO.setTransactions(pref.getString(transactions, ""));
		hubyDTO.setBalance(pref.getString(balance, ""));
		hubyDTO.setAppointments(pref.getString(appointments, ""));
		hubyDTO.setToken(pref.getString(token, ""));
		
		return hubyDTO;
	}

	public  void setName(String name) {
		pref.edit().putString(HubySharedprefrence.name,name).commit();
	}

	public  void  setEmail(String email) {
		
		pref.edit().putString(HubySharedprefrence.email,email).commit();
	}
	
	
	public  String getZipcode() {
		
		return pref.getString(lastzipcode,"365560");
	}

	public  void  setZipcode(String zipcode) {
		pref.edit().putString(HubySharedprefrence.lastzipcode,zipcode).commit();
	}
	
	
	
	
	
	
	
	
//	static public String getStringPreference(Context c, String key) {
//		SharedPreferences settings = c.getSharedPreferences(PREF_NAME,
//				Context.MODE_PRIVATE);
//		String value = settings.getString(key, "");
//		return value;
//	}
//	
	
	

}
